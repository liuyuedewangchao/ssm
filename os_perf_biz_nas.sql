/*
SQLyog Ultimate v12.09 (64 bit)
MySQL - 12.2.0-NFJD-log : Database - monitor_alarm_openstack
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`monitor_alarm_openstack` /*!40100 DEFAULT CHARACTER SET utf8 */;

USE `monitor_alarm_openstack`;

/*Table structure for table `os_perf_biz_nas` */

CREATE TABLE `os_perf_biz_nas` (
  `CREATE_TIME` datetime NOT NULL COMMENT '性能采集时间',
  `SHARE_ID` varchar(100) NOT NULL COMMENT '资源标识ID，文件系统ID',
  `AVAIL_SPACE` varchar(100) DEFAULT NULL COMMENT '当前文件系统的总容量',
  `USED_SPACE` varchar(100) DEFAULT NULL COMMENT '当前文件系统已使用的容量'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='云NAS性能数据表';

/*Data for the table `os_perf_biz_nas` */

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

/*
-- Query: SELECT * FROM helloword.qrtz_cron_triggers
LIMIT 0, 1000

-- Date: 2018-06-25 21:38
*/
INSERT INTO `qrtz_cron_triggers` (`SCHED_NAME`,`TRIGGER_NAME`,`TRIGGER_GROUP`,`CRON_EXPRESSION`,`TIME_ZONE_ID`) VALUES ('apiQuartzScheduler','exportXmlJobTrigger','DEFAULT','0 08 15 * * ?','GMT+08:00');
INSERT INTO `qrtz_cron_triggers` (`SCHED_NAME`,`TRIGGER_NAME`,`TRIGGER_GROUP`,`CRON_EXPRESSION`,`TIME_ZONE_ID`) VALUES ('apiQuartzScheduler','exportXmlJobTrigger1','DEFAULT','0 08 15 * * ?','GMT+08:00');
INSERT INTO `qrtz_cron_triggers` (`SCHED_NAME`,`TRIGGER_NAME`,`TRIGGER_GROUP`,`CRON_EXPRESSION`,`TIME_ZONE_ID`) VALUES ('apiQuartzScheduler','printNameJobDetail2Trigger','DEFAULT','0 08 15 * * ?','GMT+08:00');
INSERT INTO `qrtz_cron_triggers` (`SCHED_NAME`,`TRIGGER_NAME`,`TRIGGER_GROUP`,`CRON_EXPRESSION`,`TIME_ZONE_ID`) VALUES ('apiQuartzScheduler','printNameJobDetailTrigger','DEFAULT','0/5 * * * * ?','GMT+08:00');
INSERT INTO `qrtz_cron_triggers` (`SCHED_NAME`,`TRIGGER_NAME`,`TRIGGER_GROUP`,`CRON_EXPRESSION`,`TIME_ZONE_ID`) VALUES ('apiQuartzScheduler','rollbackOrderStatusTrigger','DEFAULT','0/5 * * * * ?','GMT+08:00');

/*
-- Query: SELECT * FROM quartz_openstack.save_perf_qrtz_locks
LIMIT 0, 1000

-- Date: 2018-06-25 22:31
*/
INSERT INTO `save_perf_qrtz_locks` (`SCHED_NAME`,`LOCK_NAME`) VALUES ('apiQuartzScheduler','STATE_ACCESS');
INSERT INTO `save_perf_qrtz_locks` (`SCHED_NAME`,`LOCK_NAME`) VALUES ('apiQuartzScheduler','TRIGGER_ACCESS');
INSERT INTO `save_perf_qrtz_locks` (`SCHED_NAME`,`LOCK_NAME`) VALUES ('quartzScheduler','STATE_ACCESS');
INSERT INTO `save_perf_qrtz_locks` (`SCHED_NAME`,`LOCK_NAME`) VALUES ('quartzScheduler','TRIGGER_ACCESS');

/*
-- Query: SELECT * FROM helloword.qrtz_job_details
LIMIT 0, 1000

-- Date: 2018-06-25 21:38
*/
INSERT INTO `qrtz_job_details` (`SCHED_NAME`,`JOB_NAME`,`JOB_GROUP`,`DESCRIPTION`,`JOB_CLASS_NAME`,`IS_DURABLE`,`IS_NONCONCURRENT`,`IS_UPDATE_DATA`,`REQUESTS_RECOVERY`,`JOB_DATA`) VALUES ('apiQuartzScheduler','exportXmlJob','DEFAULT',NULL,'org.ssm.demo.schedule.exportXmlJob','1','0','0','0',?);
INSERT INTO `qrtz_job_details` (`SCHED_NAME`,`JOB_NAME`,`JOB_GROUP`,`DESCRIPTION`,`JOB_CLASS_NAME`,`IS_DURABLE`,`IS_NONCONCURRENT`,`IS_UPDATE_DATA`,`REQUESTS_RECOVERY`,`JOB_DATA`) VALUES ('apiQuartzScheduler','printNameJobDetail','DEFAULT',NULL,'org.ssm.demo.schedule.printNameJob','1','0','0','0',?);
INSERT INTO `qrtz_job_details` (`SCHED_NAME`,`JOB_NAME`,`JOB_GROUP`,`DESCRIPTION`,`JOB_CLASS_NAME`,`IS_DURABLE`,`IS_NONCONCURRENT`,`IS_UPDATE_DATA`,`REQUESTS_RECOVERY`,`JOB_DATA`) VALUES ('apiQuartzScheduler','printNameJobDetail2','DEFAULT',NULL,'org.ssm.demo.schedule.quartz.MyDetailQuartzJobBean','1','0','0','0',?);
INSERT INTO `qrtz_job_details` (`SCHED_NAME`,`JOB_NAME`,`JOB_GROUP`,`DESCRIPTION`,`JOB_CLASS_NAME`,`IS_DURABLE`,`IS_NONCONCURRENT`,`IS_UPDATE_DATA`,`REQUESTS_RECOVERY`,`JOB_DATA`) VALUES ('apiQuartzScheduler','rollbackOrderStatus','DEFAULT',NULL,'org.ssm.demo.schedule.printNameJob','1','0','0','0',?);

<%@ page contentType="text/html; charset=utf-8"%>
<!doctype html>
<html>
<head>
    <title>新人注册</title>

    <meta http-equiv="pragma" content="no-cache">
    <meta http-equiv="cache-control" content="no-cache">
    <meta http-equiv="expires" content="0">
    <meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
    <meta http-equiv="description" content="新人注册">

    <script src="js/jquery.js"></script>
    <%--<script type="text/javascript" src="http://cdn.static.runoob.com/libs/jquery/2.1.1/jquery.min.js"></script>--%>

</head>
<body>
<h2>Hello World!my jar</h2>
<div>
    <button type="button" onclick="jump()">
        我是个按钮！按我！!
    </button>
    <button type="button" onclick="exp2()">
        按我导出！！！
    </button>

    <table class="table" border="0" cellpadding="0" cellspacing="0" id="table_vmList">
        <thead>
            <tr class="thead">
                <!--新增多选框-wangyu-2018.5.29-->
                <!--****************************start***************************-->
                <th class="vm_row_option">
                    <label class="checkbox_template">
                        <input onclick="control_cbk(this)" type="checkbox" class="hidden-input"  id="items_all"/>
                        <span class="checkbox_span"></span>
                    </label>
                </th>
                <!--****************************end***************************-->
                <th>主机名</th>
                <th>内存使用率(%)</th>
            </tr>
        </thead>

        <tbody id="policylist">
            <tr>
                <td>
                    <label class="checkbox_template">
                        <input type="checkbox" class="hidden-input"  vmname="111" value="222" name="items" />
                        <span class="checkbox_span"></span>
                    </label>
                </td>
                <td><a id="023a8bc6-23c8-4694-81b7-f95456308a69" class="activator blue" href="">test62</a></td>
                <td>试试的1</td>
            </tr>
            <tr>
                <td>
                    <label class="checkbox_template">
                        <input type="checkbox" class="hidden-input"  vmname="111" value="222" name="items" />
                        <span class="checkbox_span"></span>
                    </label>
                </td>
                <td><a id="023a8bc6-23c8-4694-fsed-f95456308a69" class="activator blue" href="">test88888</a></td>
                <td>试试的2</td>
            </tr>
            <tr>
                <td>
                    <label class="checkbox_template">
                        <input type="checkbox" class="hidden-input"  vmname="111" value="222" name="items" />
                        <span class="checkbox_span"></span>
                    </label>
                </td>
                <td><a id="023a8bc6-23c8-3461-81b7-f95456308a69" class="activator blue" href="">test</a></td>
                <td>试试的3</td>
            </tr>
        </tbody>

        <tfoot>
            <tr>
                <td colspan="8">
                    <div class="Pagination" id="vmListPage">
                        <div id="pagediv" class="pagediv"></div>
                    </div>
                </td>
            </tr>
        </tfoot>
    </table>
</div>
<script>
    function jump() {
        $.ajax({
            type:"post",
            url:"showname",
            data:{uid:"2"},
            async:false,
            error:function(){
                alert("人员不存在，请联系管理员！！！");
            },
            success:function(data){
                alert(data);
                // window.location.href = "jsp/showName.jsp";
            }
        });
        // window.location.href="http://blog.sina.com.cn/mleavs";
    }

    function exp() {
        var timeline = ["","today","yesterday","week","month"];
        var title = "云主机123";//导出的文件名样例：物理机【testIronic】20180530性能数据.xls，暂时
        title = encodeURIComponent(encodeURIComponent(title));
        var dataUrl = "monitor/exportselected";
        var vmIds = new Array("d8646b44-4420-4cd4-8aa5-da4f9e2c05fb","d8246b44-4120-4cd4-8aa5-da4f382c05fb")
        $.ajax({
            url: dataUrl,
            type: "GET",
            async: false,
            data: {
                nodeId: "test_node",
                exportType: "vm",
                dateType: timeline[1],
                params: vmIds.join(','),
                fileName: title
            },
            success: function(data) {
                alert("success！！！");
            },
            error: function(data){
            }
        });
    }

    function exp2() {
        var ids = new Array();
        var names = new Array();
        $("#table_vmList tbody tr").each(function(){
            var thisischecked=$(this).find("input[name='items']").is(":checked");
            if(thisischecked == true){
                var thisid=$(this).find("a:eq(0)").attr("ID");
                var thisname=$(this).find("a:eq(0)").html();
                alert(thisname);
                ids.push(thisid);
                names.push(thisname);
            }
        });

        var timeline = ["","today","yesterday","week","month"];
        var title = "云主机性能数据";//导出的文件名样例：物理机【testIronic】20180530性能数据.xls，暂时
        title = encodeURIComponent(encodeURIComponent(title));
        var dataUrl = "monitor/exportselected";
        var vmIds = new Array("d8646b44-4420-4cd4-8aa5-da4f9e2c05fb","d8246b44-4120-4cd4-8aa5-da4f382c05fb");

        var url = "${basePath}/monitor/exportselected?nodeId=test_node&exportType=vm&dateType="+timeline[1]+"&params="+ids.join(',')+"&params_name="+names.join(',')+"&fileName="+title;

        if(ids.length ==0 && names.length ==0){
            alert("请选择需要导出的数据");
        }else {
            // alert(ids.join(","));
            // alert(names.join(","));
            if(confirm("若监控指标较多或选择的导出周期较长，则需要等待一定时间。请确认是否导出已选周期内的实例监控数据？")){
                window.location.href = url;
            }
        }

    }


    function control_cbk(obj){
        if($(obj).is(":checked")){
            $("#table_vmList tbody tr input[name='items']").prop("checked",true);
        }else{
            $("#table_vmList tbody tr input[name='items']").prop("checked",false);
        }
    }
</script>
</body>
</html>

package org.ssm.demo.utils;

/**
 * @Author: Junerain
 * @Description:
 * @Date:Created in 11:02 2018/5/3
 * @Modified By:
 * 返回每个线程的数据下标始末，限制最大线程数
 * @param size 总数
 * @param minSize 单个线程最小执行数量
 * @param maxTask 最大线程数
 * @return
 */
public class ThreadUtils {
    public static int[] getIndex(int size, int minSize, int maxTask) {
        int listIndexCount;
        double sizeDb = (double) size, minSizeDb = (double) minSize, maxTaskDb = (double) maxTask;
        if (sizeDb / minSizeDb < maxTaskDb) {
            listIndexCount = Double.valueOf(Math.ceil(sizeDb / minSizeDb)).intValue();
        } else {
            listIndexCount = maxTask;
        }
        int each = Double.valueOf(Math.floor(sizeDb / listIndexCount)).intValue();
        int[] indexs = new int[listIndexCount + 1];
        indexs[0] = 0;
        int totalCount = 0;
        for (int i = 1; i < listIndexCount; i++) {
            indexs[i] = indexs[i - 1] + each;
            totalCount += each;
        }
        // 最后一个线程可能多分担一点
        indexs[listIndexCount] = size - totalCount + indexs[listIndexCount - 1];
        return indexs;
    }
}

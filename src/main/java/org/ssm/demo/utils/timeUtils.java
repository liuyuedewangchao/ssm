package org.ssm.demo.utils;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @Author: Junerain
 * @Description:
 * @Date:Created in 17:52 2018/5/3
 * @Modified By:
 */
public class timeUtils {
    public static String getCurrentTime(){
        Date date = new Date();
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd-hh-mm-ss");
        String time = dateFormat.format(date);
        return time;
    }

}

package org.ssm.demo.entity;

import java.util.Date;

public class DashboardCon {
    private String dashboardConId;

    private String dashboardConName;

    private String dashboardId;

    private Date createTime;

    private String resourceType;

    private String resourceId;

    private String meter;

    private String meterDesc;

    private Integer status;

    public String getDashboardConId() {
        return dashboardConId;
    }

    public void setDashboardConId(String dashboardConId) {
        this.dashboardConId = dashboardConId;
    }

    public String getDashboardConName() {
        return dashboardConName;
    }

    public void setDashboardConName(String dashboardConName) {
        this.dashboardConName = dashboardConName;
    }

    public String getDashboardId() {
        return dashboardId;
    }

    public void setDashboardId(String dashboardId) {
        this.dashboardId = dashboardId;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getResourceType() {
        return resourceType;
    }

    public void setResourceType(String resourceType) {
        this.resourceType = resourceType;
    }

    public String getResourceId() {
        return resourceId;
    }

    public void setResourceId(String resourceId) {
        this.resourceId = resourceId;
    }

    public String getMeter() {
        return meter;
    }

    public void setMeter(String meter) {
        this.meter = meter;
    }

    public String getMeterDesc() {
        return meterDesc;
    }

    public void setMeterDesc(String meterDesc) {
        this.meterDesc = meterDesc;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }
}
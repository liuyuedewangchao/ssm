package org.ssm.demo.entity;

import java.util.Date;

public class RdsMonitor {

	private Date createTime;
	private String id;
	private String cpuLoad;
	private String cpuUtilizatton;
	private String memoryUsage;
	private String iosUtilisation;
	private String iosBytesses;
	private String networkEth0;
	private String networkEth02;
	private String diskSpace;
	private String iosSec;
	private String mysqlConnects;
	private String mysqlOperations;
	public String getNetworkEth02() {
		return networkEth02;
	}
	public void setNetworkEth02(String networkEth02) {
		this.networkEth02 = networkEth02;
	}
	
	public Date getCreateTime() {
		return createTime;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getCpuLoad() {
		return cpuLoad;
	}
	public void setCpuLoad(String cpuLoad) {
		this.cpuLoad = cpuLoad;
	}
	public String getCpuUtilizatton() {
		return cpuUtilizatton;
	}
	public void setCpuUtilizatton(String cpuUtilizatton) {
		this.cpuUtilizatton = cpuUtilizatton;
	}
	public String getMemoryUsage() {
		return memoryUsage;
	}
	public void setMemoryUsage(String memoryUsage) {
		this.memoryUsage = memoryUsage;
	}
	public String getIosUtilisation() {
		return iosUtilisation;
	}
	public void setIosUtilisation(String iosUtilisation) {
		this.iosUtilisation = iosUtilisation;
	}
	public String getIosBytesses() {
		return iosBytesses;
	}
	public void setIosBytesses(String iosBytesses) {
		this.iosBytesses = iosBytesses;
	}
	public String getNetworkEth0() {
		return networkEth0;
	}
	public void setNetworkEth0(String networkEth0) {
		this.networkEth0 = networkEth0;
	}
	public String getDiskSpace() {
		return diskSpace;
	}
	public void setDiskSpace(String diskSpace) {
		this.diskSpace = diskSpace;
	}
	public String getIosSec() {
		return iosSec;
	}
	public void setIosSec(String iosSec) {
		this.iosSec = iosSec;
	}
	public String getMysqlConnects() {
		return mysqlConnects;
	}
	public void setMysqlConnects(String mysqlConnects) {
		this.mysqlConnects = mysqlConnects;
	}
	public String getMysqlOperations() {
		return mysqlOperations;
	}
	public void setMysqlOperations(String mysqlOperations) {
		this.mysqlOperations = mysqlOperations;
	}
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	
}

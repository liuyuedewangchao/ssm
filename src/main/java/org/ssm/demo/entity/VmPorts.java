package org.ssm.demo.entity;

import java.util.Date;

public class VmPorts {

    private Date createTime;
    private String userIdIpPort;
    private String statusCode;

    public Date getCreateTime() {
        return createTime;
    }
    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }
    public String getUserIdIpPort() {
        return userIdIpPort;
    }
    public void setUserIdIpPort(String userIdIpPort) {
        this.userIdIpPort = userIdIpPort;
    }
    public String getStatusCode() {
        return statusCode;
    }
    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

}

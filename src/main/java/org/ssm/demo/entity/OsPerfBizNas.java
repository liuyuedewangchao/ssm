package org.ssm.demo.entity;

import java.util.Date;

public class OsPerfBizNas {
    private Date createTime;

    private String shareId;

    private String availSpace;

    private String usedSpace;

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getShareId() {
        return shareId;
    }

    public void setShareId(String shareId) {
        this.shareId = shareId;
    }

    public String getAvailSpace() {
        return availSpace;
    }

    public void setAvailSpace(String availSpace) {
        this.availSpace = availSpace;
    }

    public String getUsedSpace() {
        return usedSpace;
    }

    public void setUsedSpace(String usedSpace) {
        this.usedSpace = usedSpace;
    }
}
package org.ssm.demo.entity;

import java.util.Date;

public class VlbMonitor {
	private String id;
	private String vlbStatus;
	private String trafficIn;
	private String trafficOut;
	private Date createTime;
	private String requestsTotal;
	private String activeCon;
	private String waitingCon;
	private String packetIn;
	private String packetOut;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getVlbStatus() {
		return vlbStatus;
	}

	public void setVlbStatus(String vlbStatus) {
		this.vlbStatus = vlbStatus;
	}

	public String getTrafficIn() {
		return trafficIn;
	}

	public void setTrafficIn(String trafficIn) {
		this.trafficIn = trafficIn;
	}

	public String getTrafficOut() {
		return trafficOut;
	}

	public void setTrafficOut(String trafficOut) {
		this.trafficOut = trafficOut;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public String getRequestsTotal() {
		return requestsTotal;
	}

	public void setRequestsTotal(String requestsTotal) {
		this.requestsTotal = requestsTotal;
	}

	public String getActiveCon() {
		return activeCon;
	}

	public void setActiveCon(String activeCon) {
		this.activeCon = activeCon;
	}

	public String getWaitingCon() {
		return waitingCon;
	}

	public void setWaitingCon(String waitingCon) {
		this.waitingCon = waitingCon;
	}

	public String getPacketIn() {
		return packetIn;
	}

	public void setPacketIn(String packetIn) {
		this.packetIn = packetIn;
	}

	public String getPacketOut() {
		return packetOut;
	}

	public void setPacketOut(String packetOut) {
		this.packetOut = packetOut;
	}

}

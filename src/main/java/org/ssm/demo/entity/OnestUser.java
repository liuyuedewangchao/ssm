package org.ssm.demo.entity;

import java.util.Date;

public class OnestUser {
	
	private Date createTime;	
	private String userId;
	//用户已创建容器的数量
	private String containerQty;
	//用户已存储对象的总数
	private String objQty;
	//总请求次数
	private String requestQty;
	//请求处理成功的次数
	private String reponseOkQty;
	//服务异常中断的次数
	private String serviceErrQty;
	//下载对象成功产生的流量,单位为MByte
	private String downSuccFlow;
	//下载对象失败产生的流量,单位为MByte
	private String downErrFlow;
//	/用户已使用空间大小，单位为MByte
	private String spaceSize;
	private String upSuccFlow;
	
	public Date getCreateTime() {
		return createTime;
	}
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getContainerQty() {
		return containerQty;
	}
	public void setContainerQty(String containerQty) {
		this.containerQty = containerQty;
	}
	public String getObjQty() {
		return objQty;
	}
	public void setObjQty(String objQty) {
		this.objQty = objQty;
	}
	public String getRequestQty() {
		return requestQty;
	}
	public void setRequestQty(String requestQty) {
		this.requestQty = requestQty;
	}
	public String getReponseOkQty() {
		return reponseOkQty;
	}
	public void setReponseOkQty(String reponseOkQty) {
		this.reponseOkQty = reponseOkQty;
	}
	public String getServiceErrQty() {
		return serviceErrQty;
	}
	public void setServiceErrQty(String serviceErrQty) {
		this.serviceErrQty = serviceErrQty;
	}
	public String getDownSuccFlow() {
		return downSuccFlow;
	}
	public void setDownSuccFlow(String downSuccFlow) {
		this.downSuccFlow = downSuccFlow;
	}
	public String getDownErrFlow() {
		return downErrFlow;
	}
	public void setDownErrFlow(String downErrFlow) {
		this.downErrFlow = downErrFlow;
	}
	public String getSpaceSize() {
		return spaceSize;
	}
	public void setSpaceSize(String spaceSize) {
		this.spaceSize = spaceSize;
	}
	public String getUpSuccFlow() {
		return upSuccFlow;
	}
	public void setUpSuccFlow(String upSuccFlow) {
		this.upSuccFlow = upSuccFlow;
	}

}

package org.ssm.demo.entity;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class OsBizNasShareExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public OsBizNasShareExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andIdIsNull() {
            addCriterion("ID is null");
            return (Criteria) this;
        }

        public Criteria andIdIsNotNull() {
            addCriterion("ID is not null");
            return (Criteria) this;
        }

        public Criteria andIdEqualTo(String value) {
            addCriterion("ID =", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotEqualTo(String value) {
            addCriterion("ID <>", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThan(String value) {
            addCriterion("ID >", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThanOrEqualTo(String value) {
            addCriterion("ID >=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThan(String value) {
            addCriterion("ID <", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThanOrEqualTo(String value) {
            addCriterion("ID <=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLike(String value) {
            addCriterion("ID like", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotLike(String value) {
            addCriterion("ID not like", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdIn(List<String> values) {
            addCriterion("ID in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotIn(List<String> values) {
            addCriterion("ID not in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdBetween(String value1, String value2) {
            addCriterion("ID between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotBetween(String value1, String value2) {
            addCriterion("ID not between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andNameIsNull() {
            addCriterion("NAME is null");
            return (Criteria) this;
        }

        public Criteria andNameIsNotNull() {
            addCriterion("NAME is not null");
            return (Criteria) this;
        }

        public Criteria andNameEqualTo(String value) {
            addCriterion("NAME =", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameNotEqualTo(String value) {
            addCriterion("NAME <>", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameGreaterThan(String value) {
            addCriterion("NAME >", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameGreaterThanOrEqualTo(String value) {
            addCriterion("NAME >=", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameLessThan(String value) {
            addCriterion("NAME <", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameLessThanOrEqualTo(String value) {
            addCriterion("NAME <=", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameLike(String value) {
            addCriterion("NAME like", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameNotLike(String value) {
            addCriterion("NAME not like", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameIn(List<String> values) {
            addCriterion("NAME in", values, "name");
            return (Criteria) this;
        }

        public Criteria andNameNotIn(List<String> values) {
            addCriterion("NAME not in", values, "name");
            return (Criteria) this;
        }

        public Criteria andNameBetween(String value1, String value2) {
            addCriterion("NAME between", value1, value2, "name");
            return (Criteria) this;
        }

        public Criteria andNameNotBetween(String value1, String value2) {
            addCriterion("NAME not between", value1, value2, "name");
            return (Criteria) this;
        }

        public Criteria andProtocolIsNull() {
            addCriterion("PROTOCOL is null");
            return (Criteria) this;
        }

        public Criteria andProtocolIsNotNull() {
            addCriterion("PROTOCOL is not null");
            return (Criteria) this;
        }

        public Criteria andProtocolEqualTo(String value) {
            addCriterion("PROTOCOL =", value, "protocol");
            return (Criteria) this;
        }

        public Criteria andProtocolNotEqualTo(String value) {
            addCriterion("PROTOCOL <>", value, "protocol");
            return (Criteria) this;
        }

        public Criteria andProtocolGreaterThan(String value) {
            addCriterion("PROTOCOL >", value, "protocol");
            return (Criteria) this;
        }

        public Criteria andProtocolGreaterThanOrEqualTo(String value) {
            addCriterion("PROTOCOL >=", value, "protocol");
            return (Criteria) this;
        }

        public Criteria andProtocolLessThan(String value) {
            addCriterion("PROTOCOL <", value, "protocol");
            return (Criteria) this;
        }

        public Criteria andProtocolLessThanOrEqualTo(String value) {
            addCriterion("PROTOCOL <=", value, "protocol");
            return (Criteria) this;
        }

        public Criteria andProtocolLike(String value) {
            addCriterion("PROTOCOL like", value, "protocol");
            return (Criteria) this;
        }

        public Criteria andProtocolNotLike(String value) {
            addCriterion("PROTOCOL not like", value, "protocol");
            return (Criteria) this;
        }

        public Criteria andProtocolIn(List<String> values) {
            addCriterion("PROTOCOL in", values, "protocol");
            return (Criteria) this;
        }

        public Criteria andProtocolNotIn(List<String> values) {
            addCriterion("PROTOCOL not in", values, "protocol");
            return (Criteria) this;
        }

        public Criteria andProtocolBetween(String value1, String value2) {
            addCriterion("PROTOCOL between", value1, value2, "protocol");
            return (Criteria) this;
        }

        public Criteria andProtocolNotBetween(String value1, String value2) {
            addCriterion("PROTOCOL not between", value1, value2, "protocol");
            return (Criteria) this;
        }

        public Criteria andSizeIsNull() {
            addCriterion("SIZE is null");
            return (Criteria) this;
        }

        public Criteria andSizeIsNotNull() {
            addCriterion("SIZE is not null");
            return (Criteria) this;
        }

        public Criteria andSizeEqualTo(Integer value) {
            addCriterion("SIZE =", value, "size");
            return (Criteria) this;
        }

        public Criteria andSizeNotEqualTo(Integer value) {
            addCriterion("SIZE <>", value, "size");
            return (Criteria) this;
        }

        public Criteria andSizeGreaterThan(Integer value) {
            addCriterion("SIZE >", value, "size");
            return (Criteria) this;
        }

        public Criteria andSizeGreaterThanOrEqualTo(Integer value) {
            addCriterion("SIZE >=", value, "size");
            return (Criteria) this;
        }

        public Criteria andSizeLessThan(Integer value) {
            addCriterion("SIZE <", value, "size");
            return (Criteria) this;
        }

        public Criteria andSizeLessThanOrEqualTo(Integer value) {
            addCriterion("SIZE <=", value, "size");
            return (Criteria) this;
        }

        public Criteria andSizeIn(List<Integer> values) {
            addCriterion("SIZE in", values, "size");
            return (Criteria) this;
        }

        public Criteria andSizeNotIn(List<Integer> values) {
            addCriterion("SIZE not in", values, "size");
            return (Criteria) this;
        }

        public Criteria andSizeBetween(Integer value1, Integer value2) {
            addCriterion("SIZE between", value1, value2, "size");
            return (Criteria) this;
        }

        public Criteria andSizeNotBetween(Integer value1, Integer value2) {
            addCriterion("SIZE not between", value1, value2, "size");
            return (Criteria) this;
        }

        public Criteria andStatusIsNull() {
            addCriterion("STATUS is null");
            return (Criteria) this;
        }

        public Criteria andStatusIsNotNull() {
            addCriterion("STATUS is not null");
            return (Criteria) this;
        }

        public Criteria andStatusEqualTo(Integer value) {
            addCriterion("STATUS =", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusNotEqualTo(Integer value) {
            addCriterion("STATUS <>", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusGreaterThan(Integer value) {
            addCriterion("STATUS >", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusGreaterThanOrEqualTo(Integer value) {
            addCriterion("STATUS >=", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusLessThan(Integer value) {
            addCriterion("STATUS <", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusLessThanOrEqualTo(Integer value) {
            addCriterion("STATUS <=", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusIn(List<Integer> values) {
            addCriterion("STATUS in", values, "status");
            return (Criteria) this;
        }

        public Criteria andStatusNotIn(List<Integer> values) {
            addCriterion("STATUS not in", values, "status");
            return (Criteria) this;
        }

        public Criteria andStatusBetween(Integer value1, Integer value2) {
            addCriterion("STATUS between", value1, value2, "status");
            return (Criteria) this;
        }

        public Criteria andStatusNotBetween(Integer value1, Integer value2) {
            addCriterion("STATUS not between", value1, value2, "status");
            return (Criteria) this;
        }

        public Criteria andDescriptionIsNull() {
            addCriterion("DESCRIPTION is null");
            return (Criteria) this;
        }

        public Criteria andDescriptionIsNotNull() {
            addCriterion("DESCRIPTION is not null");
            return (Criteria) this;
        }

        public Criteria andDescriptionEqualTo(String value) {
            addCriterion("DESCRIPTION =", value, "description");
            return (Criteria) this;
        }

        public Criteria andDescriptionNotEqualTo(String value) {
            addCriterion("DESCRIPTION <>", value, "description");
            return (Criteria) this;
        }

        public Criteria andDescriptionGreaterThan(String value) {
            addCriterion("DESCRIPTION >", value, "description");
            return (Criteria) this;
        }

        public Criteria andDescriptionGreaterThanOrEqualTo(String value) {
            addCriterion("DESCRIPTION >=", value, "description");
            return (Criteria) this;
        }

        public Criteria andDescriptionLessThan(String value) {
            addCriterion("DESCRIPTION <", value, "description");
            return (Criteria) this;
        }

        public Criteria andDescriptionLessThanOrEqualTo(String value) {
            addCriterion("DESCRIPTION <=", value, "description");
            return (Criteria) this;
        }

        public Criteria andDescriptionLike(String value) {
            addCriterion("DESCRIPTION like", value, "description");
            return (Criteria) this;
        }

        public Criteria andDescriptionNotLike(String value) {
            addCriterion("DESCRIPTION not like", value, "description");
            return (Criteria) this;
        }

        public Criteria andDescriptionIn(List<String> values) {
            addCriterion("DESCRIPTION in", values, "description");
            return (Criteria) this;
        }

        public Criteria andDescriptionNotIn(List<String> values) {
            addCriterion("DESCRIPTION not in", values, "description");
            return (Criteria) this;
        }

        public Criteria andDescriptionBetween(String value1, String value2) {
            addCriterion("DESCRIPTION between", value1, value2, "description");
            return (Criteria) this;
        }

        public Criteria andDescriptionNotBetween(String value1, String value2) {
            addCriterion("DESCRIPTION not between", value1, value2, "description");
            return (Criteria) this;
        }

        public Criteria andShareTypeIsNull() {
            addCriterion("SHARE_TYPE is null");
            return (Criteria) this;
        }

        public Criteria andShareTypeIsNotNull() {
            addCriterion("SHARE_TYPE is not null");
            return (Criteria) this;
        }

        public Criteria andShareTypeEqualTo(String value) {
            addCriterion("SHARE_TYPE =", value, "shareType");
            return (Criteria) this;
        }

        public Criteria andShareTypeNotEqualTo(String value) {
            addCriterion("SHARE_TYPE <>", value, "shareType");
            return (Criteria) this;
        }

        public Criteria andShareTypeGreaterThan(String value) {
            addCriterion("SHARE_TYPE >", value, "shareType");
            return (Criteria) this;
        }

        public Criteria andShareTypeGreaterThanOrEqualTo(String value) {
            addCriterion("SHARE_TYPE >=", value, "shareType");
            return (Criteria) this;
        }

        public Criteria andShareTypeLessThan(String value) {
            addCriterion("SHARE_TYPE <", value, "shareType");
            return (Criteria) this;
        }

        public Criteria andShareTypeLessThanOrEqualTo(String value) {
            addCriterion("SHARE_TYPE <=", value, "shareType");
            return (Criteria) this;
        }

        public Criteria andShareTypeLike(String value) {
            addCriterion("SHARE_TYPE like", value, "shareType");
            return (Criteria) this;
        }

        public Criteria andShareTypeNotLike(String value) {
            addCriterion("SHARE_TYPE not like", value, "shareType");
            return (Criteria) this;
        }

        public Criteria andShareTypeIn(List<String> values) {
            addCriterion("SHARE_TYPE in", values, "shareType");
            return (Criteria) this;
        }

        public Criteria andShareTypeNotIn(List<String> values) {
            addCriterion("SHARE_TYPE not in", values, "shareType");
            return (Criteria) this;
        }

        public Criteria andShareTypeBetween(String value1, String value2) {
            addCriterion("SHARE_TYPE between", value1, value2, "shareType");
            return (Criteria) this;
        }

        public Criteria andShareTypeNotBetween(String value1, String value2) {
            addCriterion("SHARE_TYPE not between", value1, value2, "shareType");
            return (Criteria) this;
        }

        public Criteria andShareTypeNameIsNull() {
            addCriterion("SHARE_TYPE_NAME is null");
            return (Criteria) this;
        }

        public Criteria andShareTypeNameIsNotNull() {
            addCriterion("SHARE_TYPE_NAME is not null");
            return (Criteria) this;
        }

        public Criteria andShareTypeNameEqualTo(String value) {
            addCriterion("SHARE_TYPE_NAME =", value, "shareTypeName");
            return (Criteria) this;
        }

        public Criteria andShareTypeNameNotEqualTo(String value) {
            addCriterion("SHARE_TYPE_NAME <>", value, "shareTypeName");
            return (Criteria) this;
        }

        public Criteria andShareTypeNameGreaterThan(String value) {
            addCriterion("SHARE_TYPE_NAME >", value, "shareTypeName");
            return (Criteria) this;
        }

        public Criteria andShareTypeNameGreaterThanOrEqualTo(String value) {
            addCriterion("SHARE_TYPE_NAME >=", value, "shareTypeName");
            return (Criteria) this;
        }

        public Criteria andShareTypeNameLessThan(String value) {
            addCriterion("SHARE_TYPE_NAME <", value, "shareTypeName");
            return (Criteria) this;
        }

        public Criteria andShareTypeNameLessThanOrEqualTo(String value) {
            addCriterion("SHARE_TYPE_NAME <=", value, "shareTypeName");
            return (Criteria) this;
        }

        public Criteria andShareTypeNameLike(String value) {
            addCriterion("SHARE_TYPE_NAME like", value, "shareTypeName");
            return (Criteria) this;
        }

        public Criteria andShareTypeNameNotLike(String value) {
            addCriterion("SHARE_TYPE_NAME not like", value, "shareTypeName");
            return (Criteria) this;
        }

        public Criteria andShareTypeNameIn(List<String> values) {
            addCriterion("SHARE_TYPE_NAME in", values, "shareTypeName");
            return (Criteria) this;
        }

        public Criteria andShareTypeNameNotIn(List<String> values) {
            addCriterion("SHARE_TYPE_NAME not in", values, "shareTypeName");
            return (Criteria) this;
        }

        public Criteria andShareTypeNameBetween(String value1, String value2) {
            addCriterion("SHARE_TYPE_NAME between", value1, value2, "shareTypeName");
            return (Criteria) this;
        }

        public Criteria andShareTypeNameNotBetween(String value1, String value2) {
            addCriterion("SHARE_TYPE_NAME not between", value1, value2, "shareTypeName");
            return (Criteria) this;
        }

        public Criteria andVolumeTypeIsNull() {
            addCriterion("VOLUME_TYPE is null");
            return (Criteria) this;
        }

        public Criteria andVolumeTypeIsNotNull() {
            addCriterion("VOLUME_TYPE is not null");
            return (Criteria) this;
        }

        public Criteria andVolumeTypeEqualTo(String value) {
            addCriterion("VOLUME_TYPE =", value, "volumeType");
            return (Criteria) this;
        }

        public Criteria andVolumeTypeNotEqualTo(String value) {
            addCriterion("VOLUME_TYPE <>", value, "volumeType");
            return (Criteria) this;
        }

        public Criteria andVolumeTypeGreaterThan(String value) {
            addCriterion("VOLUME_TYPE >", value, "volumeType");
            return (Criteria) this;
        }

        public Criteria andVolumeTypeGreaterThanOrEqualTo(String value) {
            addCriterion("VOLUME_TYPE >=", value, "volumeType");
            return (Criteria) this;
        }

        public Criteria andVolumeTypeLessThan(String value) {
            addCriterion("VOLUME_TYPE <", value, "volumeType");
            return (Criteria) this;
        }

        public Criteria andVolumeTypeLessThanOrEqualTo(String value) {
            addCriterion("VOLUME_TYPE <=", value, "volumeType");
            return (Criteria) this;
        }

        public Criteria andVolumeTypeLike(String value) {
            addCriterion("VOLUME_TYPE like", value, "volumeType");
            return (Criteria) this;
        }

        public Criteria andVolumeTypeNotLike(String value) {
            addCriterion("VOLUME_TYPE not like", value, "volumeType");
            return (Criteria) this;
        }

        public Criteria andVolumeTypeIn(List<String> values) {
            addCriterion("VOLUME_TYPE in", values, "volumeType");
            return (Criteria) this;
        }

        public Criteria andVolumeTypeNotIn(List<String> values) {
            addCriterion("VOLUME_TYPE not in", values, "volumeType");
            return (Criteria) this;
        }

        public Criteria andVolumeTypeBetween(String value1, String value2) {
            addCriterion("VOLUME_TYPE between", value1, value2, "volumeType");
            return (Criteria) this;
        }

        public Criteria andVolumeTypeNotBetween(String value1, String value2) {
            addCriterion("VOLUME_TYPE not between", value1, value2, "volumeType");
            return (Criteria) this;
        }

        public Criteria andSnapshotIdIsNull() {
            addCriterion("SNAPSHOT_ID is null");
            return (Criteria) this;
        }

        public Criteria andSnapshotIdIsNotNull() {
            addCriterion("SNAPSHOT_ID is not null");
            return (Criteria) this;
        }

        public Criteria andSnapshotIdEqualTo(String value) {
            addCriterion("SNAPSHOT_ID =", value, "snapshotId");
            return (Criteria) this;
        }

        public Criteria andSnapshotIdNotEqualTo(String value) {
            addCriterion("SNAPSHOT_ID <>", value, "snapshotId");
            return (Criteria) this;
        }

        public Criteria andSnapshotIdGreaterThan(String value) {
            addCriterion("SNAPSHOT_ID >", value, "snapshotId");
            return (Criteria) this;
        }

        public Criteria andSnapshotIdGreaterThanOrEqualTo(String value) {
            addCriterion("SNAPSHOT_ID >=", value, "snapshotId");
            return (Criteria) this;
        }

        public Criteria andSnapshotIdLessThan(String value) {
            addCriterion("SNAPSHOT_ID <", value, "snapshotId");
            return (Criteria) this;
        }

        public Criteria andSnapshotIdLessThanOrEqualTo(String value) {
            addCriterion("SNAPSHOT_ID <=", value, "snapshotId");
            return (Criteria) this;
        }

        public Criteria andSnapshotIdLike(String value) {
            addCriterion("SNAPSHOT_ID like", value, "snapshotId");
            return (Criteria) this;
        }

        public Criteria andSnapshotIdNotLike(String value) {
            addCriterion("SNAPSHOT_ID not like", value, "snapshotId");
            return (Criteria) this;
        }

        public Criteria andSnapshotIdIn(List<String> values) {
            addCriterion("SNAPSHOT_ID in", values, "snapshotId");
            return (Criteria) this;
        }

        public Criteria andSnapshotIdNotIn(List<String> values) {
            addCriterion("SNAPSHOT_ID not in", values, "snapshotId");
            return (Criteria) this;
        }

        public Criteria andSnapshotIdBetween(String value1, String value2) {
            addCriterion("SNAPSHOT_ID between", value1, value2, "snapshotId");
            return (Criteria) this;
        }

        public Criteria andSnapshotIdNotBetween(String value1, String value2) {
            addCriterion("SNAPSHOT_ID not between", value1, value2, "snapshotId");
            return (Criteria) this;
        }

        public Criteria andIsPublicIsNull() {
            addCriterion("IS_PUBLIC is null");
            return (Criteria) this;
        }

        public Criteria andIsPublicIsNotNull() {
            addCriterion("IS_PUBLIC is not null");
            return (Criteria) this;
        }

        public Criteria andIsPublicEqualTo(Boolean value) {
            addCriterion("IS_PUBLIC =", value, "isPublic");
            return (Criteria) this;
        }

        public Criteria andIsPublicNotEqualTo(Boolean value) {
            addCriterion("IS_PUBLIC <>", value, "isPublic");
            return (Criteria) this;
        }

        public Criteria andIsPublicGreaterThan(Boolean value) {
            addCriterion("IS_PUBLIC >", value, "isPublic");
            return (Criteria) this;
        }

        public Criteria andIsPublicGreaterThanOrEqualTo(Boolean value) {
            addCriterion("IS_PUBLIC >=", value, "isPublic");
            return (Criteria) this;
        }

        public Criteria andIsPublicLessThan(Boolean value) {
            addCriterion("IS_PUBLIC <", value, "isPublic");
            return (Criteria) this;
        }

        public Criteria andIsPublicLessThanOrEqualTo(Boolean value) {
            addCriterion("IS_PUBLIC <=", value, "isPublic");
            return (Criteria) this;
        }

        public Criteria andIsPublicIn(List<Boolean> values) {
            addCriterion("IS_PUBLIC in", values, "isPublic");
            return (Criteria) this;
        }

        public Criteria andIsPublicNotIn(List<Boolean> values) {
            addCriterion("IS_PUBLIC not in", values, "isPublic");
            return (Criteria) this;
        }

        public Criteria andIsPublicBetween(Boolean value1, Boolean value2) {
            addCriterion("IS_PUBLIC between", value1, value2, "isPublic");
            return (Criteria) this;
        }

        public Criteria andIsPublicNotBetween(Boolean value1, Boolean value2) {
            addCriterion("IS_PUBLIC not between", value1, value2, "isPublic");
            return (Criteria) this;
        }

        public Criteria andShareNetworkIdIsNull() {
            addCriterion("SHARE_NETWORK_ID is null");
            return (Criteria) this;
        }

        public Criteria andShareNetworkIdIsNotNull() {
            addCriterion("SHARE_NETWORK_ID is not null");
            return (Criteria) this;
        }

        public Criteria andShareNetworkIdEqualTo(String value) {
            addCriterion("SHARE_NETWORK_ID =", value, "shareNetworkId");
            return (Criteria) this;
        }

        public Criteria andShareNetworkIdNotEqualTo(String value) {
            addCriterion("SHARE_NETWORK_ID <>", value, "shareNetworkId");
            return (Criteria) this;
        }

        public Criteria andShareNetworkIdGreaterThan(String value) {
            addCriterion("SHARE_NETWORK_ID >", value, "shareNetworkId");
            return (Criteria) this;
        }

        public Criteria andShareNetworkIdGreaterThanOrEqualTo(String value) {
            addCriterion("SHARE_NETWORK_ID >=", value, "shareNetworkId");
            return (Criteria) this;
        }

        public Criteria andShareNetworkIdLessThan(String value) {
            addCriterion("SHARE_NETWORK_ID <", value, "shareNetworkId");
            return (Criteria) this;
        }

        public Criteria andShareNetworkIdLessThanOrEqualTo(String value) {
            addCriterion("SHARE_NETWORK_ID <=", value, "shareNetworkId");
            return (Criteria) this;
        }

        public Criteria andShareNetworkIdLike(String value) {
            addCriterion("SHARE_NETWORK_ID like", value, "shareNetworkId");
            return (Criteria) this;
        }

        public Criteria andShareNetworkIdNotLike(String value) {
            addCriterion("SHARE_NETWORK_ID not like", value, "shareNetworkId");
            return (Criteria) this;
        }

        public Criteria andShareNetworkIdIn(List<String> values) {
            addCriterion("SHARE_NETWORK_ID in", values, "shareNetworkId");
            return (Criteria) this;
        }

        public Criteria andShareNetworkIdNotIn(List<String> values) {
            addCriterion("SHARE_NETWORK_ID not in", values, "shareNetworkId");
            return (Criteria) this;
        }

        public Criteria andShareNetworkIdBetween(String value1, String value2) {
            addCriterion("SHARE_NETWORK_ID between", value1, value2, "shareNetworkId");
            return (Criteria) this;
        }

        public Criteria andShareNetworkIdNotBetween(String value1, String value2) {
            addCriterion("SHARE_NETWORK_ID not between", value1, value2, "shareNetworkId");
            return (Criteria) this;
        }

        public Criteria andAvailabilityZoneIsNull() {
            addCriterion("AVAILABILITY_ZONE is null");
            return (Criteria) this;
        }

        public Criteria andAvailabilityZoneIsNotNull() {
            addCriterion("AVAILABILITY_ZONE is not null");
            return (Criteria) this;
        }

        public Criteria andAvailabilityZoneEqualTo(String value) {
            addCriterion("AVAILABILITY_ZONE =", value, "availabilityZone");
            return (Criteria) this;
        }

        public Criteria andAvailabilityZoneNotEqualTo(String value) {
            addCriterion("AVAILABILITY_ZONE <>", value, "availabilityZone");
            return (Criteria) this;
        }

        public Criteria andAvailabilityZoneGreaterThan(String value) {
            addCriterion("AVAILABILITY_ZONE >", value, "availabilityZone");
            return (Criteria) this;
        }

        public Criteria andAvailabilityZoneGreaterThanOrEqualTo(String value) {
            addCriterion("AVAILABILITY_ZONE >=", value, "availabilityZone");
            return (Criteria) this;
        }

        public Criteria andAvailabilityZoneLessThan(String value) {
            addCriterion("AVAILABILITY_ZONE <", value, "availabilityZone");
            return (Criteria) this;
        }

        public Criteria andAvailabilityZoneLessThanOrEqualTo(String value) {
            addCriterion("AVAILABILITY_ZONE <=", value, "availabilityZone");
            return (Criteria) this;
        }

        public Criteria andAvailabilityZoneLike(String value) {
            addCriterion("AVAILABILITY_ZONE like", value, "availabilityZone");
            return (Criteria) this;
        }

        public Criteria andAvailabilityZoneNotLike(String value) {
            addCriterion("AVAILABILITY_ZONE not like", value, "availabilityZone");
            return (Criteria) this;
        }

        public Criteria andAvailabilityZoneIn(List<String> values) {
            addCriterion("AVAILABILITY_ZONE in", values, "availabilityZone");
            return (Criteria) this;
        }

        public Criteria andAvailabilityZoneNotIn(List<String> values) {
            addCriterion("AVAILABILITY_ZONE not in", values, "availabilityZone");
            return (Criteria) this;
        }

        public Criteria andAvailabilityZoneBetween(String value1, String value2) {
            addCriterion("AVAILABILITY_ZONE between", value1, value2, "availabilityZone");
            return (Criteria) this;
        }

        public Criteria andAvailabilityZoneNotBetween(String value1, String value2) {
            addCriterion("AVAILABILITY_ZONE not between", value1, value2, "availabilityZone");
            return (Criteria) this;
        }

        public Criteria andShareHostNameIsNull() {
            addCriterion("SHARE_HOST_NAME is null");
            return (Criteria) this;
        }

        public Criteria andShareHostNameIsNotNull() {
            addCriterion("SHARE_HOST_NAME is not null");
            return (Criteria) this;
        }

        public Criteria andShareHostNameEqualTo(String value) {
            addCriterion("SHARE_HOST_NAME =", value, "shareHostName");
            return (Criteria) this;
        }

        public Criteria andShareHostNameNotEqualTo(String value) {
            addCriterion("SHARE_HOST_NAME <>", value, "shareHostName");
            return (Criteria) this;
        }

        public Criteria andShareHostNameGreaterThan(String value) {
            addCriterion("SHARE_HOST_NAME >", value, "shareHostName");
            return (Criteria) this;
        }

        public Criteria andShareHostNameGreaterThanOrEqualTo(String value) {
            addCriterion("SHARE_HOST_NAME >=", value, "shareHostName");
            return (Criteria) this;
        }

        public Criteria andShareHostNameLessThan(String value) {
            addCriterion("SHARE_HOST_NAME <", value, "shareHostName");
            return (Criteria) this;
        }

        public Criteria andShareHostNameLessThanOrEqualTo(String value) {
            addCriterion("SHARE_HOST_NAME <=", value, "shareHostName");
            return (Criteria) this;
        }

        public Criteria andShareHostNameLike(String value) {
            addCriterion("SHARE_HOST_NAME like", value, "shareHostName");
            return (Criteria) this;
        }

        public Criteria andShareHostNameNotLike(String value) {
            addCriterion("SHARE_HOST_NAME not like", value, "shareHostName");
            return (Criteria) this;
        }

        public Criteria andShareHostNameIn(List<String> values) {
            addCriterion("SHARE_HOST_NAME in", values, "shareHostName");
            return (Criteria) this;
        }

        public Criteria andShareHostNameNotIn(List<String> values) {
            addCriterion("SHARE_HOST_NAME not in", values, "shareHostName");
            return (Criteria) this;
        }

        public Criteria andShareHostNameBetween(String value1, String value2) {
            addCriterion("SHARE_HOST_NAME between", value1, value2, "shareHostName");
            return (Criteria) this;
        }

        public Criteria andShareHostNameNotBetween(String value1, String value2) {
            addCriterion("SHARE_HOST_NAME not between", value1, value2, "shareHostName");
            return (Criteria) this;
        }

        public Criteria andExportLocationIsNull() {
            addCriterion("EXPORT_LOCATION is null");
            return (Criteria) this;
        }

        public Criteria andExportLocationIsNotNull() {
            addCriterion("EXPORT_LOCATION is not null");
            return (Criteria) this;
        }

        public Criteria andExportLocationEqualTo(String value) {
            addCriterion("EXPORT_LOCATION =", value, "exportLocation");
            return (Criteria) this;
        }

        public Criteria andExportLocationNotEqualTo(String value) {
            addCriterion("EXPORT_LOCATION <>", value, "exportLocation");
            return (Criteria) this;
        }

        public Criteria andExportLocationGreaterThan(String value) {
            addCriterion("EXPORT_LOCATION >", value, "exportLocation");
            return (Criteria) this;
        }

        public Criteria andExportLocationGreaterThanOrEqualTo(String value) {
            addCriterion("EXPORT_LOCATION >=", value, "exportLocation");
            return (Criteria) this;
        }

        public Criteria andExportLocationLessThan(String value) {
            addCriterion("EXPORT_LOCATION <", value, "exportLocation");
            return (Criteria) this;
        }

        public Criteria andExportLocationLessThanOrEqualTo(String value) {
            addCriterion("EXPORT_LOCATION <=", value, "exportLocation");
            return (Criteria) this;
        }

        public Criteria andExportLocationLike(String value) {
            addCriterion("EXPORT_LOCATION like", value, "exportLocation");
            return (Criteria) this;
        }

        public Criteria andExportLocationNotLike(String value) {
            addCriterion("EXPORT_LOCATION not like", value, "exportLocation");
            return (Criteria) this;
        }

        public Criteria andExportLocationIn(List<String> values) {
            addCriterion("EXPORT_LOCATION in", values, "exportLocation");
            return (Criteria) this;
        }

        public Criteria andExportLocationNotIn(List<String> values) {
            addCriterion("EXPORT_LOCATION not in", values, "exportLocation");
            return (Criteria) this;
        }

        public Criteria andExportLocationBetween(String value1, String value2) {
            addCriterion("EXPORT_LOCATION between", value1, value2, "exportLocation");
            return (Criteria) this;
        }

        public Criteria andExportLocationNotBetween(String value1, String value2) {
            addCriterion("EXPORT_LOCATION not between", value1, value2, "exportLocation");
            return (Criteria) this;
        }

        public Criteria andTaskStateIsNull() {
            addCriterion("TASK_STATE is null");
            return (Criteria) this;
        }

        public Criteria andTaskStateIsNotNull() {
            addCriterion("TASK_STATE is not null");
            return (Criteria) this;
        }

        public Criteria andTaskStateEqualTo(Integer value) {
            addCriterion("TASK_STATE =", value, "taskState");
            return (Criteria) this;
        }

        public Criteria andTaskStateNotEqualTo(Integer value) {
            addCriterion("TASK_STATE <>", value, "taskState");
            return (Criteria) this;
        }

        public Criteria andTaskStateGreaterThan(Integer value) {
            addCriterion("TASK_STATE >", value, "taskState");
            return (Criteria) this;
        }

        public Criteria andTaskStateGreaterThanOrEqualTo(Integer value) {
            addCriterion("TASK_STATE >=", value, "taskState");
            return (Criteria) this;
        }

        public Criteria andTaskStateLessThan(Integer value) {
            addCriterion("TASK_STATE <", value, "taskState");
            return (Criteria) this;
        }

        public Criteria andTaskStateLessThanOrEqualTo(Integer value) {
            addCriterion("TASK_STATE <=", value, "taskState");
            return (Criteria) this;
        }

        public Criteria andTaskStateIn(List<Integer> values) {
            addCriterion("TASK_STATE in", values, "taskState");
            return (Criteria) this;
        }

        public Criteria andTaskStateNotIn(List<Integer> values) {
            addCriterion("TASK_STATE not in", values, "taskState");
            return (Criteria) this;
        }

        public Criteria andTaskStateBetween(Integer value1, Integer value2) {
            addCriterion("TASK_STATE between", value1, value2, "taskState");
            return (Criteria) this;
        }

        public Criteria andTaskStateNotBetween(Integer value1, Integer value2) {
            addCriterion("TASK_STATE not between", value1, value2, "taskState");
            return (Criteria) this;
        }

        public Criteria andShareServerIdIsNull() {
            addCriterion("SHARE_SERVER_ID is null");
            return (Criteria) this;
        }

        public Criteria andShareServerIdIsNotNull() {
            addCriterion("SHARE_SERVER_ID is not null");
            return (Criteria) this;
        }

        public Criteria andShareServerIdEqualTo(String value) {
            addCriterion("SHARE_SERVER_ID =", value, "shareServerId");
            return (Criteria) this;
        }

        public Criteria andShareServerIdNotEqualTo(String value) {
            addCriterion("SHARE_SERVER_ID <>", value, "shareServerId");
            return (Criteria) this;
        }

        public Criteria andShareServerIdGreaterThan(String value) {
            addCriterion("SHARE_SERVER_ID >", value, "shareServerId");
            return (Criteria) this;
        }

        public Criteria andShareServerIdGreaterThanOrEqualTo(String value) {
            addCriterion("SHARE_SERVER_ID >=", value, "shareServerId");
            return (Criteria) this;
        }

        public Criteria andShareServerIdLessThan(String value) {
            addCriterion("SHARE_SERVER_ID <", value, "shareServerId");
            return (Criteria) this;
        }

        public Criteria andShareServerIdLessThanOrEqualTo(String value) {
            addCriterion("SHARE_SERVER_ID <=", value, "shareServerId");
            return (Criteria) this;
        }

        public Criteria andShareServerIdLike(String value) {
            addCriterion("SHARE_SERVER_ID like", value, "shareServerId");
            return (Criteria) this;
        }

        public Criteria andShareServerIdNotLike(String value) {
            addCriterion("SHARE_SERVER_ID not like", value, "shareServerId");
            return (Criteria) this;
        }

        public Criteria andShareServerIdIn(List<String> values) {
            addCriterion("SHARE_SERVER_ID in", values, "shareServerId");
            return (Criteria) this;
        }

        public Criteria andShareServerIdNotIn(List<String> values) {
            addCriterion("SHARE_SERVER_ID not in", values, "shareServerId");
            return (Criteria) this;
        }

        public Criteria andShareServerIdBetween(String value1, String value2) {
            addCriterion("SHARE_SERVER_ID between", value1, value2, "shareServerId");
            return (Criteria) this;
        }

        public Criteria andShareServerIdNotBetween(String value1, String value2) {
            addCriterion("SHARE_SERVER_ID not between", value1, value2, "shareServerId");
            return (Criteria) this;
        }

        public Criteria andConsistencyGroupIdIsNull() {
            addCriterion("CONSISTENCY_GROUP_ID is null");
            return (Criteria) this;
        }

        public Criteria andConsistencyGroupIdIsNotNull() {
            addCriterion("CONSISTENCY_GROUP_ID is not null");
            return (Criteria) this;
        }

        public Criteria andConsistencyGroupIdEqualTo(String value) {
            addCriterion("CONSISTENCY_GROUP_ID =", value, "consistencyGroupId");
            return (Criteria) this;
        }

        public Criteria andConsistencyGroupIdNotEqualTo(String value) {
            addCriterion("CONSISTENCY_GROUP_ID <>", value, "consistencyGroupId");
            return (Criteria) this;
        }

        public Criteria andConsistencyGroupIdGreaterThan(String value) {
            addCriterion("CONSISTENCY_GROUP_ID >", value, "consistencyGroupId");
            return (Criteria) this;
        }

        public Criteria andConsistencyGroupIdGreaterThanOrEqualTo(String value) {
            addCriterion("CONSISTENCY_GROUP_ID >=", value, "consistencyGroupId");
            return (Criteria) this;
        }

        public Criteria andConsistencyGroupIdLessThan(String value) {
            addCriterion("CONSISTENCY_GROUP_ID <", value, "consistencyGroupId");
            return (Criteria) this;
        }

        public Criteria andConsistencyGroupIdLessThanOrEqualTo(String value) {
            addCriterion("CONSISTENCY_GROUP_ID <=", value, "consistencyGroupId");
            return (Criteria) this;
        }

        public Criteria andConsistencyGroupIdLike(String value) {
            addCriterion("CONSISTENCY_GROUP_ID like", value, "consistencyGroupId");
            return (Criteria) this;
        }

        public Criteria andConsistencyGroupIdNotLike(String value) {
            addCriterion("CONSISTENCY_GROUP_ID not like", value, "consistencyGroupId");
            return (Criteria) this;
        }

        public Criteria andConsistencyGroupIdIn(List<String> values) {
            addCriterion("CONSISTENCY_GROUP_ID in", values, "consistencyGroupId");
            return (Criteria) this;
        }

        public Criteria andConsistencyGroupIdNotIn(List<String> values) {
            addCriterion("CONSISTENCY_GROUP_ID not in", values, "consistencyGroupId");
            return (Criteria) this;
        }

        public Criteria andConsistencyGroupIdBetween(String value1, String value2) {
            addCriterion("CONSISTENCY_GROUP_ID between", value1, value2, "consistencyGroupId");
            return (Criteria) this;
        }

        public Criteria andConsistencyGroupIdNotBetween(String value1, String value2) {
            addCriterion("CONSISTENCY_GROUP_ID not between", value1, value2, "consistencyGroupId");
            return (Criteria) this;
        }

        public Criteria andSnapshotSupportIsNull() {
            addCriterion("SNAPSHOT_SUPPORT is null");
            return (Criteria) this;
        }

        public Criteria andSnapshotSupportIsNotNull() {
            addCriterion("SNAPSHOT_SUPPORT is not null");
            return (Criteria) this;
        }

        public Criteria andSnapshotSupportEqualTo(Boolean value) {
            addCriterion("SNAPSHOT_SUPPORT =", value, "snapshotSupport");
            return (Criteria) this;
        }

        public Criteria andSnapshotSupportNotEqualTo(Boolean value) {
            addCriterion("SNAPSHOT_SUPPORT <>", value, "snapshotSupport");
            return (Criteria) this;
        }

        public Criteria andSnapshotSupportGreaterThan(Boolean value) {
            addCriterion("SNAPSHOT_SUPPORT >", value, "snapshotSupport");
            return (Criteria) this;
        }

        public Criteria andSnapshotSupportGreaterThanOrEqualTo(Boolean value) {
            addCriterion("SNAPSHOT_SUPPORT >=", value, "snapshotSupport");
            return (Criteria) this;
        }

        public Criteria andSnapshotSupportLessThan(Boolean value) {
            addCriterion("SNAPSHOT_SUPPORT <", value, "snapshotSupport");
            return (Criteria) this;
        }

        public Criteria andSnapshotSupportLessThanOrEqualTo(Boolean value) {
            addCriterion("SNAPSHOT_SUPPORT <=", value, "snapshotSupport");
            return (Criteria) this;
        }

        public Criteria andSnapshotSupportIn(List<Boolean> values) {
            addCriterion("SNAPSHOT_SUPPORT in", values, "snapshotSupport");
            return (Criteria) this;
        }

        public Criteria andSnapshotSupportNotIn(List<Boolean> values) {
            addCriterion("SNAPSHOT_SUPPORT not in", values, "snapshotSupport");
            return (Criteria) this;
        }

        public Criteria andSnapshotSupportBetween(Boolean value1, Boolean value2) {
            addCriterion("SNAPSHOT_SUPPORT between", value1, value2, "snapshotSupport");
            return (Criteria) this;
        }

        public Criteria andSnapshotSupportNotBetween(Boolean value1, Boolean value2) {
            addCriterion("SNAPSHOT_SUPPORT not between", value1, value2, "snapshotSupport");
            return (Criteria) this;
        }

        public Criteria andSourceCgSnapshotMemberIdIsNull() {
            addCriterion("SOURCE_CG_SNAPSHOT_MEMBER_ID is null");
            return (Criteria) this;
        }

        public Criteria andSourceCgSnapshotMemberIdIsNotNull() {
            addCriterion("SOURCE_CG_SNAPSHOT_MEMBER_ID is not null");
            return (Criteria) this;
        }

        public Criteria andSourceCgSnapshotMemberIdEqualTo(String value) {
            addCriterion("SOURCE_CG_SNAPSHOT_MEMBER_ID =", value, "sourceCgSnapshotMemberId");
            return (Criteria) this;
        }

        public Criteria andSourceCgSnapshotMemberIdNotEqualTo(String value) {
            addCriterion("SOURCE_CG_SNAPSHOT_MEMBER_ID <>", value, "sourceCgSnapshotMemberId");
            return (Criteria) this;
        }

        public Criteria andSourceCgSnapshotMemberIdGreaterThan(String value) {
            addCriterion("SOURCE_CG_SNAPSHOT_MEMBER_ID >", value, "sourceCgSnapshotMemberId");
            return (Criteria) this;
        }

        public Criteria andSourceCgSnapshotMemberIdGreaterThanOrEqualTo(String value) {
            addCriterion("SOURCE_CG_SNAPSHOT_MEMBER_ID >=", value, "sourceCgSnapshotMemberId");
            return (Criteria) this;
        }

        public Criteria andSourceCgSnapshotMemberIdLessThan(String value) {
            addCriterion("SOURCE_CG_SNAPSHOT_MEMBER_ID <", value, "sourceCgSnapshotMemberId");
            return (Criteria) this;
        }

        public Criteria andSourceCgSnapshotMemberIdLessThanOrEqualTo(String value) {
            addCriterion("SOURCE_CG_SNAPSHOT_MEMBER_ID <=", value, "sourceCgSnapshotMemberId");
            return (Criteria) this;
        }

        public Criteria andSourceCgSnapshotMemberIdLike(String value) {
            addCriterion("SOURCE_CG_SNAPSHOT_MEMBER_ID like", value, "sourceCgSnapshotMemberId");
            return (Criteria) this;
        }

        public Criteria andSourceCgSnapshotMemberIdNotLike(String value) {
            addCriterion("SOURCE_CG_SNAPSHOT_MEMBER_ID not like", value, "sourceCgSnapshotMemberId");
            return (Criteria) this;
        }

        public Criteria andSourceCgSnapshotMemberIdIn(List<String> values) {
            addCriterion("SOURCE_CG_SNAPSHOT_MEMBER_ID in", values, "sourceCgSnapshotMemberId");
            return (Criteria) this;
        }

        public Criteria andSourceCgSnapshotMemberIdNotIn(List<String> values) {
            addCriterion("SOURCE_CG_SNAPSHOT_MEMBER_ID not in", values, "sourceCgSnapshotMemberId");
            return (Criteria) this;
        }

        public Criteria andSourceCgSnapshotMemberIdBetween(String value1, String value2) {
            addCriterion("SOURCE_CG_SNAPSHOT_MEMBER_ID between", value1, value2, "sourceCgSnapshotMemberId");
            return (Criteria) this;
        }

        public Criteria andSourceCgSnapshotMemberIdNotBetween(String value1, String value2) {
            addCriterion("SOURCE_CG_SNAPSHOT_MEMBER_ID not between", value1, value2, "sourceCgSnapshotMemberId");
            return (Criteria) this;
        }

        public Criteria andProposerIsNull() {
            addCriterion("PROPOSER is null");
            return (Criteria) this;
        }

        public Criteria andProposerIsNotNull() {
            addCriterion("PROPOSER is not null");
            return (Criteria) this;
        }

        public Criteria andProposerEqualTo(String value) {
            addCriterion("PROPOSER =", value, "proposer");
            return (Criteria) this;
        }

        public Criteria andProposerNotEqualTo(String value) {
            addCriterion("PROPOSER <>", value, "proposer");
            return (Criteria) this;
        }

        public Criteria andProposerGreaterThan(String value) {
            addCriterion("PROPOSER >", value, "proposer");
            return (Criteria) this;
        }

        public Criteria andProposerGreaterThanOrEqualTo(String value) {
            addCriterion("PROPOSER >=", value, "proposer");
            return (Criteria) this;
        }

        public Criteria andProposerLessThan(String value) {
            addCriterion("PROPOSER <", value, "proposer");
            return (Criteria) this;
        }

        public Criteria andProposerLessThanOrEqualTo(String value) {
            addCriterion("PROPOSER <=", value, "proposer");
            return (Criteria) this;
        }

        public Criteria andProposerLike(String value) {
            addCriterion("PROPOSER like", value, "proposer");
            return (Criteria) this;
        }

        public Criteria andProposerNotLike(String value) {
            addCriterion("PROPOSER not like", value, "proposer");
            return (Criteria) this;
        }

        public Criteria andProposerIn(List<String> values) {
            addCriterion("PROPOSER in", values, "proposer");
            return (Criteria) this;
        }

        public Criteria andProposerNotIn(List<String> values) {
            addCriterion("PROPOSER not in", values, "proposer");
            return (Criteria) this;
        }

        public Criteria andProposerBetween(String value1, String value2) {
            addCriterion("PROPOSER between", value1, value2, "proposer");
            return (Criteria) this;
        }

        public Criteria andProposerNotBetween(String value1, String value2) {
            addCriterion("PROPOSER not between", value1, value2, "proposer");
            return (Criteria) this;
        }

        public Criteria andCustomerIdIsNull() {
            addCriterion("CUSTOMER_ID is null");
            return (Criteria) this;
        }

        public Criteria andCustomerIdIsNotNull() {
            addCriterion("CUSTOMER_ID is not null");
            return (Criteria) this;
        }

        public Criteria andCustomerIdEqualTo(String value) {
            addCriterion("CUSTOMER_ID =", value, "customerId");
            return (Criteria) this;
        }

        public Criteria andCustomerIdNotEqualTo(String value) {
            addCriterion("CUSTOMER_ID <>", value, "customerId");
            return (Criteria) this;
        }

        public Criteria andCustomerIdGreaterThan(String value) {
            addCriterion("CUSTOMER_ID >", value, "customerId");
            return (Criteria) this;
        }

        public Criteria andCustomerIdGreaterThanOrEqualTo(String value) {
            addCriterion("CUSTOMER_ID >=", value, "customerId");
            return (Criteria) this;
        }

        public Criteria andCustomerIdLessThan(String value) {
            addCriterion("CUSTOMER_ID <", value, "customerId");
            return (Criteria) this;
        }

        public Criteria andCustomerIdLessThanOrEqualTo(String value) {
            addCriterion("CUSTOMER_ID <=", value, "customerId");
            return (Criteria) this;
        }

        public Criteria andCustomerIdLike(String value) {
            addCriterion("CUSTOMER_ID like", value, "customerId");
            return (Criteria) this;
        }

        public Criteria andCustomerIdNotLike(String value) {
            addCriterion("CUSTOMER_ID not like", value, "customerId");
            return (Criteria) this;
        }

        public Criteria andCustomerIdIn(List<String> values) {
            addCriterion("CUSTOMER_ID in", values, "customerId");
            return (Criteria) this;
        }

        public Criteria andCustomerIdNotIn(List<String> values) {
            addCriterion("CUSTOMER_ID not in", values, "customerId");
            return (Criteria) this;
        }

        public Criteria andCustomerIdBetween(String value1, String value2) {
            addCriterion("CUSTOMER_ID between", value1, value2, "customerId");
            return (Criteria) this;
        }

        public Criteria andCustomerIdNotBetween(String value1, String value2) {
            addCriterion("CUSTOMER_ID not between", value1, value2, "customerId");
            return (Criteria) this;
        }

        public Criteria andPoolIdIsNull() {
            addCriterion("POOL_ID is null");
            return (Criteria) this;
        }

        public Criteria andPoolIdIsNotNull() {
            addCriterion("POOL_ID is not null");
            return (Criteria) this;
        }

        public Criteria andPoolIdEqualTo(String value) {
            addCriterion("POOL_ID =", value, "poolId");
            return (Criteria) this;
        }

        public Criteria andPoolIdNotEqualTo(String value) {
            addCriterion("POOL_ID <>", value, "poolId");
            return (Criteria) this;
        }

        public Criteria andPoolIdGreaterThan(String value) {
            addCriterion("POOL_ID >", value, "poolId");
            return (Criteria) this;
        }

        public Criteria andPoolIdGreaterThanOrEqualTo(String value) {
            addCriterion("POOL_ID >=", value, "poolId");
            return (Criteria) this;
        }

        public Criteria andPoolIdLessThan(String value) {
            addCriterion("POOL_ID <", value, "poolId");
            return (Criteria) this;
        }

        public Criteria andPoolIdLessThanOrEqualTo(String value) {
            addCriterion("POOL_ID <=", value, "poolId");
            return (Criteria) this;
        }

        public Criteria andPoolIdLike(String value) {
            addCriterion("POOL_ID like", value, "poolId");
            return (Criteria) this;
        }

        public Criteria andPoolIdNotLike(String value) {
            addCriterion("POOL_ID not like", value, "poolId");
            return (Criteria) this;
        }

        public Criteria andPoolIdIn(List<String> values) {
            addCriterion("POOL_ID in", values, "poolId");
            return (Criteria) this;
        }

        public Criteria andPoolIdNotIn(List<String> values) {
            addCriterion("POOL_ID not in", values, "poolId");
            return (Criteria) this;
        }

        public Criteria andPoolIdBetween(String value1, String value2) {
            addCriterion("POOL_ID between", value1, value2, "poolId");
            return (Criteria) this;
        }

        public Criteria andPoolIdNotBetween(String value1, String value2) {
            addCriterion("POOL_ID not between", value1, value2, "poolId");
            return (Criteria) this;
        }

        public Criteria andCreatedByIsNull() {
            addCriterion("CREATED_BY is null");
            return (Criteria) this;
        }

        public Criteria andCreatedByIsNotNull() {
            addCriterion("CREATED_BY is not null");
            return (Criteria) this;
        }

        public Criteria andCreatedByEqualTo(String value) {
            addCriterion("CREATED_BY =", value, "createdBy");
            return (Criteria) this;
        }

        public Criteria andCreatedByNotEqualTo(String value) {
            addCriterion("CREATED_BY <>", value, "createdBy");
            return (Criteria) this;
        }

        public Criteria andCreatedByGreaterThan(String value) {
            addCriterion("CREATED_BY >", value, "createdBy");
            return (Criteria) this;
        }

        public Criteria andCreatedByGreaterThanOrEqualTo(String value) {
            addCriterion("CREATED_BY >=", value, "createdBy");
            return (Criteria) this;
        }

        public Criteria andCreatedByLessThan(String value) {
            addCriterion("CREATED_BY <", value, "createdBy");
            return (Criteria) this;
        }

        public Criteria andCreatedByLessThanOrEqualTo(String value) {
            addCriterion("CREATED_BY <=", value, "createdBy");
            return (Criteria) this;
        }

        public Criteria andCreatedByLike(String value) {
            addCriterion("CREATED_BY like", value, "createdBy");
            return (Criteria) this;
        }

        public Criteria andCreatedByNotLike(String value) {
            addCriterion("CREATED_BY not like", value, "createdBy");
            return (Criteria) this;
        }

        public Criteria andCreatedByIn(List<String> values) {
            addCriterion("CREATED_BY in", values, "createdBy");
            return (Criteria) this;
        }

        public Criteria andCreatedByNotIn(List<String> values) {
            addCriterion("CREATED_BY not in", values, "createdBy");
            return (Criteria) this;
        }

        public Criteria andCreatedByBetween(String value1, String value2) {
            addCriterion("CREATED_BY between", value1, value2, "createdBy");
            return (Criteria) this;
        }

        public Criteria andCreatedByNotBetween(String value1, String value2) {
            addCriterion("CREATED_BY not between", value1, value2, "createdBy");
            return (Criteria) this;
        }

        public Criteria andCreatedTimeIsNull() {
            addCriterion("CREATED_TIME is null");
            return (Criteria) this;
        }

        public Criteria andCreatedTimeIsNotNull() {
            addCriterion("CREATED_TIME is not null");
            return (Criteria) this;
        }

        public Criteria andCreatedTimeEqualTo(Date value) {
            addCriterion("CREATED_TIME =", value, "createdTime");
            return (Criteria) this;
        }

        public Criteria andCreatedTimeNotEqualTo(Date value) {
            addCriterion("CREATED_TIME <>", value, "createdTime");
            return (Criteria) this;
        }

        public Criteria andCreatedTimeGreaterThan(Date value) {
            addCriterion("CREATED_TIME >", value, "createdTime");
            return (Criteria) this;
        }

        public Criteria andCreatedTimeGreaterThanOrEqualTo(Date value) {
            addCriterion("CREATED_TIME >=", value, "createdTime");
            return (Criteria) this;
        }

        public Criteria andCreatedTimeLessThan(Date value) {
            addCriterion("CREATED_TIME <", value, "createdTime");
            return (Criteria) this;
        }

        public Criteria andCreatedTimeLessThanOrEqualTo(Date value) {
            addCriterion("CREATED_TIME <=", value, "createdTime");
            return (Criteria) this;
        }

        public Criteria andCreatedTimeIn(List<Date> values) {
            addCriterion("CREATED_TIME in", values, "createdTime");
            return (Criteria) this;
        }

        public Criteria andCreatedTimeNotIn(List<Date> values) {
            addCriterion("CREATED_TIME not in", values, "createdTime");
            return (Criteria) this;
        }

        public Criteria andCreatedTimeBetween(Date value1, Date value2) {
            addCriterion("CREATED_TIME between", value1, value2, "createdTime");
            return (Criteria) this;
        }

        public Criteria andCreatedTimeNotBetween(Date value1, Date value2) {
            addCriterion("CREATED_TIME not between", value1, value2, "createdTime");
            return (Criteria) this;
        }

        public Criteria andModifiedByIsNull() {
            addCriterion("MODIFIED_BY is null");
            return (Criteria) this;
        }

        public Criteria andModifiedByIsNotNull() {
            addCriterion("MODIFIED_BY is not null");
            return (Criteria) this;
        }

        public Criteria andModifiedByEqualTo(String value) {
            addCriterion("MODIFIED_BY =", value, "modifiedBy");
            return (Criteria) this;
        }

        public Criteria andModifiedByNotEqualTo(String value) {
            addCriterion("MODIFIED_BY <>", value, "modifiedBy");
            return (Criteria) this;
        }

        public Criteria andModifiedByGreaterThan(String value) {
            addCriterion("MODIFIED_BY >", value, "modifiedBy");
            return (Criteria) this;
        }

        public Criteria andModifiedByGreaterThanOrEqualTo(String value) {
            addCriterion("MODIFIED_BY >=", value, "modifiedBy");
            return (Criteria) this;
        }

        public Criteria andModifiedByLessThan(String value) {
            addCriterion("MODIFIED_BY <", value, "modifiedBy");
            return (Criteria) this;
        }

        public Criteria andModifiedByLessThanOrEqualTo(String value) {
            addCriterion("MODIFIED_BY <=", value, "modifiedBy");
            return (Criteria) this;
        }

        public Criteria andModifiedByLike(String value) {
            addCriterion("MODIFIED_BY like", value, "modifiedBy");
            return (Criteria) this;
        }

        public Criteria andModifiedByNotLike(String value) {
            addCriterion("MODIFIED_BY not like", value, "modifiedBy");
            return (Criteria) this;
        }

        public Criteria andModifiedByIn(List<String> values) {
            addCriterion("MODIFIED_BY in", values, "modifiedBy");
            return (Criteria) this;
        }

        public Criteria andModifiedByNotIn(List<String> values) {
            addCriterion("MODIFIED_BY not in", values, "modifiedBy");
            return (Criteria) this;
        }

        public Criteria andModifiedByBetween(String value1, String value2) {
            addCriterion("MODIFIED_BY between", value1, value2, "modifiedBy");
            return (Criteria) this;
        }

        public Criteria andModifiedByNotBetween(String value1, String value2) {
            addCriterion("MODIFIED_BY not between", value1, value2, "modifiedBy");
            return (Criteria) this;
        }

        public Criteria andModifiedTimeIsNull() {
            addCriterion("MODIFIED_TIME is null");
            return (Criteria) this;
        }

        public Criteria andModifiedTimeIsNotNull() {
            addCriterion("MODIFIED_TIME is not null");
            return (Criteria) this;
        }

        public Criteria andModifiedTimeEqualTo(Date value) {
            addCriterion("MODIFIED_TIME =", value, "modifiedTime");
            return (Criteria) this;
        }

        public Criteria andModifiedTimeNotEqualTo(Date value) {
            addCriterion("MODIFIED_TIME <>", value, "modifiedTime");
            return (Criteria) this;
        }

        public Criteria andModifiedTimeGreaterThan(Date value) {
            addCriterion("MODIFIED_TIME >", value, "modifiedTime");
            return (Criteria) this;
        }

        public Criteria andModifiedTimeGreaterThanOrEqualTo(Date value) {
            addCriterion("MODIFIED_TIME >=", value, "modifiedTime");
            return (Criteria) this;
        }

        public Criteria andModifiedTimeLessThan(Date value) {
            addCriterion("MODIFIED_TIME <", value, "modifiedTime");
            return (Criteria) this;
        }

        public Criteria andModifiedTimeLessThanOrEqualTo(Date value) {
            addCriterion("MODIFIED_TIME <=", value, "modifiedTime");
            return (Criteria) this;
        }

        public Criteria andModifiedTimeIn(List<Date> values) {
            addCriterion("MODIFIED_TIME in", values, "modifiedTime");
            return (Criteria) this;
        }

        public Criteria andModifiedTimeNotIn(List<Date> values) {
            addCriterion("MODIFIED_TIME not in", values, "modifiedTime");
            return (Criteria) this;
        }

        public Criteria andModifiedTimeBetween(Date value1, Date value2) {
            addCriterion("MODIFIED_TIME between", value1, value2, "modifiedTime");
            return (Criteria) this;
        }

        public Criteria andModifiedTimeNotBetween(Date value1, Date value2) {
            addCriterion("MODIFIED_TIME not between", value1, value2, "modifiedTime");
            return (Criteria) this;
        }

        public Criteria andIsDeleteIsNull() {
            addCriterion("IS_DELETE is null");
            return (Criteria) this;
        }

        public Criteria andIsDeleteIsNotNull() {
            addCriterion("IS_DELETE is not null");
            return (Criteria) this;
        }

        public Criteria andIsDeleteEqualTo(Integer value) {
            addCriterion("IS_DELETE =", value, "isDelete");
            return (Criteria) this;
        }

        public Criteria andIsDeleteNotEqualTo(Integer value) {
            addCriterion("IS_DELETE <>", value, "isDelete");
            return (Criteria) this;
        }

        public Criteria andIsDeleteGreaterThan(Integer value) {
            addCriterion("IS_DELETE >", value, "isDelete");
            return (Criteria) this;
        }

        public Criteria andIsDeleteGreaterThanOrEqualTo(Integer value) {
            addCriterion("IS_DELETE >=", value, "isDelete");
            return (Criteria) this;
        }

        public Criteria andIsDeleteLessThan(Integer value) {
            addCriterion("IS_DELETE <", value, "isDelete");
            return (Criteria) this;
        }

        public Criteria andIsDeleteLessThanOrEqualTo(Integer value) {
            addCriterion("IS_DELETE <=", value, "isDelete");
            return (Criteria) this;
        }

        public Criteria andIsDeleteIn(List<Integer> values) {
            addCriterion("IS_DELETE in", values, "isDelete");
            return (Criteria) this;
        }

        public Criteria andIsDeleteNotIn(List<Integer> values) {
            addCriterion("IS_DELETE not in", values, "isDelete");
            return (Criteria) this;
        }

        public Criteria andIsDeleteBetween(Integer value1, Integer value2) {
            addCriterion("IS_DELETE between", value1, value2, "isDelete");
            return (Criteria) this;
        }

        public Criteria andIsDeleteNotBetween(Integer value1, Integer value2) {
            addCriterion("IS_DELETE not between", value1, value2, "isDelete");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}
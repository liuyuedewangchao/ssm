package org.ssm.demo.entity;

import java.util.Date;

public class OsBizNasShare {
    private String id;

    private String name;

    private String protocol;

    private Integer size;

    private Integer status;

    private String description;

    private String shareType;

    private String shareTypeName;

    private String volumeType;

    private String snapshotId;

    private Boolean isPublic;

    private String shareNetworkId;

    private String availabilityZone;

    private String shareHostName;

    private String exportLocation;

    private Integer taskState;

    private String shareServerId;

    private String consistencyGroupId;

    private Boolean snapshotSupport;

    private String sourceCgSnapshotMemberId;

    private String proposer;

    private String customerId;

    private String poolId;

    private String createdBy;

    private Date createdTime;

    private String modifiedBy;

    private Date modifiedTime;

    private Integer isDelete;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getProtocol() {
        return protocol;
    }

    public void setProtocol(String protocol) {
        this.protocol = protocol;
    }

    public Integer getSize() {
        return size;
    }

    public void setSize(Integer size) {
        this.size = size;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getShareType() {
        return shareType;
    }

    public void setShareType(String shareType) {
        this.shareType = shareType;
    }

    public String getShareTypeName() {
        return shareTypeName;
    }

    public void setShareTypeName(String shareTypeName) {
        this.shareTypeName = shareTypeName;
    }

    public String getVolumeType() {
        return volumeType;
    }

    public void setVolumeType(String volumeType) {
        this.volumeType = volumeType;
    }

    public String getSnapshotId() {
        return snapshotId;
    }

    public void setSnapshotId(String snapshotId) {
        this.snapshotId = snapshotId;
    }

    public Boolean getIsPublic() {
        return isPublic;
    }

    public void setIsPublic(Boolean isPublic) {
        this.isPublic = isPublic;
    }

    public String getShareNetworkId() {
        return shareNetworkId;
    }

    public void setShareNetworkId(String shareNetworkId) {
        this.shareNetworkId = shareNetworkId;
    }

    public String getAvailabilityZone() {
        return availabilityZone;
    }

    public void setAvailabilityZone(String availabilityZone) {
        this.availabilityZone = availabilityZone;
    }

    public String getShareHostName() {
        return shareHostName;
    }

    public void setShareHostName(String shareHostName) {
        this.shareHostName = shareHostName;
    }

    public String getExportLocation() {
        return exportLocation;
    }

    public void setExportLocation(String exportLocation) {
        this.exportLocation = exportLocation;
    }

    public Integer getTaskState() {
        return taskState;
    }

    public void setTaskState(Integer taskState) {
        this.taskState = taskState;
    }

    public String getShareServerId() {
        return shareServerId;
    }

    public void setShareServerId(String shareServerId) {
        this.shareServerId = shareServerId;
    }

    public String getConsistencyGroupId() {
        return consistencyGroupId;
    }

    public void setConsistencyGroupId(String consistencyGroupId) {
        this.consistencyGroupId = consistencyGroupId;
    }

    public Boolean getSnapshotSupport() {
        return snapshotSupport;
    }

    public void setSnapshotSupport(Boolean snapshotSupport) {
        this.snapshotSupport = snapshotSupport;
    }

    public String getSourceCgSnapshotMemberId() {
        return sourceCgSnapshotMemberId;
    }

    public void setSourceCgSnapshotMemberId(String sourceCgSnapshotMemberId) {
        this.sourceCgSnapshotMemberId = sourceCgSnapshotMemberId;
    }

    public String getProposer() {
        return proposer;
    }

    public void setProposer(String proposer) {
        this.proposer = proposer;
    }

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public String getPoolId() {
        return poolId;
    }

    public void setPoolId(String poolId) {
        this.poolId = poolId;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Date getCreatedTime() {
        return createdTime;
    }

    public void setCreatedTime(Date createdTime) {
        this.createdTime = createdTime;
    }

    public String getModifiedBy() {
        return modifiedBy;
    }

    public void setModifiedBy(String modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

    public Date getModifiedTime() {
        return modifiedTime;
    }

    public void setModifiedTime(Date modifiedTime) {
        this.modifiedTime = modifiedTime;
    }

    public Integer getIsDelete() {
        return isDelete;
    }

    public void setIsDelete(Integer isDelete) {
        this.isDelete = isDelete;
    }
}
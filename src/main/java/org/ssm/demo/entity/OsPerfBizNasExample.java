package org.ssm.demo.entity;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class OsPerfBizNasExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public OsPerfBizNasExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andCreateTimeIsNull() {
            addCriterion("CREATE_TIME is null");
            return (Criteria) this;
        }

        public Criteria andCreateTimeIsNotNull() {
            addCriterion("CREATE_TIME is not null");
            return (Criteria) this;
        }

        public Criteria andCreateTimeEqualTo(Date value) {
            addCriterion("CREATE_TIME =", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeNotEqualTo(Date value) {
            addCriterion("CREATE_TIME <>", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeGreaterThan(Date value) {
            addCriterion("CREATE_TIME >", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeGreaterThanOrEqualTo(Date value) {
            addCriterion("CREATE_TIME >=", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeLessThan(Date value) {
            addCriterion("CREATE_TIME <", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeLessThanOrEqualTo(Date value) {
            addCriterion("CREATE_TIME <=", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeIn(List<Date> values) {
            addCriterion("CREATE_TIME in", values, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeNotIn(List<Date> values) {
            addCriterion("CREATE_TIME not in", values, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeBetween(Date value1, Date value2) {
            addCriterion("CREATE_TIME between", value1, value2, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeNotBetween(Date value1, Date value2) {
            addCriterion("CREATE_TIME not between", value1, value2, "createTime");
            return (Criteria) this;
        }

        public Criteria andShareIdIsNull() {
            addCriterion("SHARE_ID is null");
            return (Criteria) this;
        }

        public Criteria andShareIdIsNotNull() {
            addCriterion("SHARE_ID is not null");
            return (Criteria) this;
        }

        public Criteria andShareIdEqualTo(String value) {
            addCriterion("SHARE_ID =", value, "shareId");
            return (Criteria) this;
        }

        public Criteria andShareIdNotEqualTo(String value) {
            addCriterion("SHARE_ID <>", value, "shareId");
            return (Criteria) this;
        }

        public Criteria andShareIdGreaterThan(String value) {
            addCriterion("SHARE_ID >", value, "shareId");
            return (Criteria) this;
        }

        public Criteria andShareIdGreaterThanOrEqualTo(String value) {
            addCriterion("SHARE_ID >=", value, "shareId");
            return (Criteria) this;
        }

        public Criteria andShareIdLessThan(String value) {
            addCriterion("SHARE_ID <", value, "shareId");
            return (Criteria) this;
        }

        public Criteria andShareIdLessThanOrEqualTo(String value) {
            addCriterion("SHARE_ID <=", value, "shareId");
            return (Criteria) this;
        }

        public Criteria andShareIdLike(String value) {
            addCriterion("SHARE_ID like", value, "shareId");
            return (Criteria) this;
        }

        public Criteria andShareIdNotLike(String value) {
            addCriterion("SHARE_ID not like", value, "shareId");
            return (Criteria) this;
        }

        public Criteria andShareIdIn(List<String> values) {
            addCriterion("SHARE_ID in", values, "shareId");
            return (Criteria) this;
        }

        public Criteria andShareIdNotIn(List<String> values) {
            addCriterion("SHARE_ID not in", values, "shareId");
            return (Criteria) this;
        }

        public Criteria andShareIdBetween(String value1, String value2) {
            addCriterion("SHARE_ID between", value1, value2, "shareId");
            return (Criteria) this;
        }

        public Criteria andShareIdNotBetween(String value1, String value2) {
            addCriterion("SHARE_ID not between", value1, value2, "shareId");
            return (Criteria) this;
        }

        public Criteria andAvailSpaceIsNull() {
            addCriterion("AVAIL_SPACE is null");
            return (Criteria) this;
        }

        public Criteria andAvailSpaceIsNotNull() {
            addCriterion("AVAIL_SPACE is not null");
            return (Criteria) this;
        }

        public Criteria andAvailSpaceEqualTo(String value) {
            addCriterion("AVAIL_SPACE =", value, "availSpace");
            return (Criteria) this;
        }

        public Criteria andAvailSpaceNotEqualTo(String value) {
            addCriterion("AVAIL_SPACE <>", value, "availSpace");
            return (Criteria) this;
        }

        public Criteria andAvailSpaceGreaterThan(String value) {
            addCriterion("AVAIL_SPACE >", value, "availSpace");
            return (Criteria) this;
        }

        public Criteria andAvailSpaceGreaterThanOrEqualTo(String value) {
            addCriterion("AVAIL_SPACE >=", value, "availSpace");
            return (Criteria) this;
        }

        public Criteria andAvailSpaceLessThan(String value) {
            addCriterion("AVAIL_SPACE <", value, "availSpace");
            return (Criteria) this;
        }

        public Criteria andAvailSpaceLessThanOrEqualTo(String value) {
            addCriterion("AVAIL_SPACE <=", value, "availSpace");
            return (Criteria) this;
        }

        public Criteria andAvailSpaceLike(String value) {
            addCriterion("AVAIL_SPACE like", value, "availSpace");
            return (Criteria) this;
        }

        public Criteria andAvailSpaceNotLike(String value) {
            addCriterion("AVAIL_SPACE not like", value, "availSpace");
            return (Criteria) this;
        }

        public Criteria andAvailSpaceIn(List<String> values) {
            addCriterion("AVAIL_SPACE in", values, "availSpace");
            return (Criteria) this;
        }

        public Criteria andAvailSpaceNotIn(List<String> values) {
            addCriterion("AVAIL_SPACE not in", values, "availSpace");
            return (Criteria) this;
        }

        public Criteria andAvailSpaceBetween(String value1, String value2) {
            addCriterion("AVAIL_SPACE between", value1, value2, "availSpace");
            return (Criteria) this;
        }

        public Criteria andAvailSpaceNotBetween(String value1, String value2) {
            addCriterion("AVAIL_SPACE not between", value1, value2, "availSpace");
            return (Criteria) this;
        }

        public Criteria andUsedSpaceIsNull() {
            addCriterion("USED_SPACE is null");
            return (Criteria) this;
        }

        public Criteria andUsedSpaceIsNotNull() {
            addCriterion("USED_SPACE is not null");
            return (Criteria) this;
        }

        public Criteria andUsedSpaceEqualTo(String value) {
            addCriterion("USED_SPACE =", value, "usedSpace");
            return (Criteria) this;
        }

        public Criteria andUsedSpaceNotEqualTo(String value) {
            addCriterion("USED_SPACE <>", value, "usedSpace");
            return (Criteria) this;
        }

        public Criteria andUsedSpaceGreaterThan(String value) {
            addCriterion("USED_SPACE >", value, "usedSpace");
            return (Criteria) this;
        }

        public Criteria andUsedSpaceGreaterThanOrEqualTo(String value) {
            addCriterion("USED_SPACE >=", value, "usedSpace");
            return (Criteria) this;
        }

        public Criteria andUsedSpaceLessThan(String value) {
            addCriterion("USED_SPACE <", value, "usedSpace");
            return (Criteria) this;
        }

        public Criteria andUsedSpaceLessThanOrEqualTo(String value) {
            addCriterion("USED_SPACE <=", value, "usedSpace");
            return (Criteria) this;
        }

        public Criteria andUsedSpaceLike(String value) {
            addCriterion("USED_SPACE like", value, "usedSpace");
            return (Criteria) this;
        }

        public Criteria andUsedSpaceNotLike(String value) {
            addCriterion("USED_SPACE not like", value, "usedSpace");
            return (Criteria) this;
        }

        public Criteria andUsedSpaceIn(List<String> values) {
            addCriterion("USED_SPACE in", values, "usedSpace");
            return (Criteria) this;
        }

        public Criteria andUsedSpaceNotIn(List<String> values) {
            addCriterion("USED_SPACE not in", values, "usedSpace");
            return (Criteria) this;
        }

        public Criteria andUsedSpaceBetween(String value1, String value2) {
            addCriterion("USED_SPACE between", value1, value2, "usedSpace");
            return (Criteria) this;
        }

        public Criteria andUsedSpaceNotBetween(String value1, String value2) {
            addCriterion("USED_SPACE not between", value1, value2, "usedSpace");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}
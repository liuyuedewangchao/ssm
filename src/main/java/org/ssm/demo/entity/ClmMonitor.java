package org.ssm.demo.entity;

import java.util.Date;

/**
 * 
 * 物理机性能数据
 * @author xxx
 *
 */
public class ClmMonitor
{
	// 性能采集时间
	private Date create_time;
	// 物理机id
	private String pm_id;
	// 'cpu利用率 单位（%）',
	private String cpu_idle;
	// '启动的空闲cpu百分比 单位（%）',
	private String cpu_aidle;
	// '用户空间占用cpu百分比 单位（%）',
	private String cpu_user;
	// '用户进程空间内改变过优先级的进程占用cpu百分比 单位（%）',
	private String cpu_nice;
	// '内核空间占用cpu百分比 单位（%）',
	private String cpu_system;
	// '物理内存总量（kbs显示） 单位（kb）',
	private String mem_total;
	// '缓存内存大小 单位（kb）',
	private String mem_cached;
	// '空闲内存大小 单位（kb）',
	private String mem_free;
	// '共享内存大小 单位（kb）',
	private String mem_shared;
	// '内存利用率 单位（%）',
	private String mem_percent;
	// '内核缓存的内存总量 单位（kb）',
	private String mem_buffers;
	// '交换分区总量（kbs显示）单位（kb）',
	private String swap_total;
	// '空闲交换分区大小 单位（%）',
	private String swap_free;
	// 'swap利用率 单位（kb）',
	private String swap_percent;
	// '磁盘总大小 单位（gb）',
	private String disk_total;
	// '剩余磁盘空间 单位（gb）',
	private String disk_free;
	// '磁盘利用率 单位（%）',
	private String disk_percent;
	// '磁盘io利用率（汇总）单位（%）',
	private String diskstat_all_percent_io_time;
	// '磁盘io利用率 单位（%）',
	private String diskstat_sda_percent_io_time;
	// '系统磁盘写入速率（汇总）单位（byte/s）',
	private String diskstat_all_write_bytes_per_sec;
	// '系统磁盘读取速率（汇总）单位（byte/s）',
	private String diskstat_all_read_bytes_per_sec;
	// '系统磁盘写入速率 单位（byte/s）',
	private String diskstat_sda_write_bytes_per_sec;
	// '系统磁盘读取速率 单位（byte/s）',
	private String diskstat_sda_read_bytes_per_sec;
	// '运行的进程总数 单位（个）',
	private String proc_run;
	// '进程总数 单位（个）',
	private String proc_total;
	// '每分钟的系统平均负载 单位（个）',
	private String load_one;
	// '每5分钟的系统平均负载 单位（个）',
	private String load_five;
	// '每15分钟的系统平均负载 单位（个）',
	private String load_fifteen;
	// '每秒进来的包 单位（packets/sec）',
	private String pkts_in;
	// '每秒出去的包 单位（packets/sec）',
	private String pkts_out;
	// '网络入口带宽速度 单位（bytes/sec）',
	private String bytes_in;
	// '网络出口带宽速度（新增）单位（bytes/sec）',
	private String bytes_out;
	// '物理机ID（OP用',
	private String bm_id;

	public Date getCreate_time() {
		return create_time;
	}

	public void setCreate_time(Date create_time) {
		this.create_time = create_time;
	}

	public String getPm_id() {
		return pm_id;
	}

	public void setPm_id(String pm_id) {
		this.pm_id = pm_id;
	}

	public String getCpu_idle() {
		return cpu_idle;
	}

	public void setCpu_idle(String cpu_idle) {
		this.cpu_idle = cpu_idle;
	}

	public String getCpu_aidle() {
		return cpu_aidle;
	}

	public void setCpu_aidle(String cpu_aidle) {
		this.cpu_aidle = cpu_aidle;
	}

	public String getCpu_user() {
		return cpu_user;
	}

	public void setCpu_user(String cpu_user) {
		this.cpu_user = cpu_user;
	}

	public String getCpu_nice() {
		return cpu_nice;
	}

	public void setCpu_nice(String cpu_nice) {
		this.cpu_nice = cpu_nice;
	}

	public String getCpu_system() {
		return cpu_system;
	}

	public void setCpu_system(String cpu_system) {
		this.cpu_system = cpu_system;
	}

	public String getMem_total() {
		return mem_total;
	}

	public void setMem_total(String mem_total) {
		this.mem_total = mem_total;
	}

	public String getMem_cached() {
		return mem_cached;
	}

	public void setMem_cached(String mem_cached) {
		this.mem_cached = mem_cached;
	}

	public String getMem_free() {
		return mem_free;
	}

	public void setMem_free(String mem_free) {
		this.mem_free = mem_free;
	}

	public String getMem_shared() {
		return mem_shared;
	}

	public void setMem_shared(String mem_shared) {
		this.mem_shared = mem_shared;
	}

	public String getMem_percent() {
		return mem_percent;
	}

	public void setMem_percent(String mem_percent) {
		this.mem_percent = mem_percent;
	}

	public String getMem_buffers() {
		return mem_buffers;
	}

	public void setMem_buffers(String mem_buffers) {
		this.mem_buffers = mem_buffers;
	}

	public String getSwap_total() {
		return swap_total;
	}

	public void setSwap_total(String swap_total) {
		this.swap_total = swap_total;
	}

	public String getSwap_free() {
		return swap_free;
	}

	public void setSwap_free(String swap_free) {
		this.swap_free = swap_free;
	}

	public String getSwap_percent() {
		return swap_percent;
	}

	public void setSwap_percent(String swap_percent) {
		this.swap_percent = swap_percent;
	}

	public String getDisk_total() {
		return disk_total;
	}

	public void setDisk_total(String disk_total) {
		this.disk_total = disk_total;
	}

	public String getDisk_free() {
		return disk_free;
	}

	public void setDisk_free(String disk_free) {
		this.disk_free = disk_free;
	}

	public String getDisk_percent() {
		return disk_percent;
	}

	public void setDisk_percent(String disk_percent) {
		this.disk_percent = disk_percent;
	}

	public String getDiskstat_all_percent_io_time() {
		return diskstat_all_percent_io_time;
	}

	public void setDiskstat_all_percent_io_time(String diskstat_all_percent_io_time) {
		this.diskstat_all_percent_io_time = diskstat_all_percent_io_time;
	}

	public String getDiskstat_sda_percent_io_time() {
		return diskstat_sda_percent_io_time;
	}

	public void setDiskstat_sda_percent_io_time(String diskstat_sda_percent_io_time) {
		this.diskstat_sda_percent_io_time = diskstat_sda_percent_io_time;
	}

	public String getDiskstat_all_write_bytes_per_sec() {
		return diskstat_all_write_bytes_per_sec;
	}

	public void setDiskstat_all_write_bytes_per_sec(String diskstat_all_write_bytes_per_sec) {
		this.diskstat_all_write_bytes_per_sec = diskstat_all_write_bytes_per_sec;
	}

	public String getDiskstat_all_read_bytes_per_sec() {
		return diskstat_all_read_bytes_per_sec;
	}

	public void setDiskstat_all_read_bytes_per_sec(String diskstat_all_read_bytes_per_sec) {
		this.diskstat_all_read_bytes_per_sec = diskstat_all_read_bytes_per_sec;
	}

	public String getDiskstat_sda_write_bytes_per_sec() {
		return diskstat_sda_write_bytes_per_sec;
	}

	public void setDiskstat_sda_write_bytes_per_sec(String diskstat_sda_write_bytes_per_sec) {
		this.diskstat_sda_write_bytes_per_sec = diskstat_sda_write_bytes_per_sec;
	}

	public String getDiskstat_sda_read_bytes_per_sec() {
		return diskstat_sda_read_bytes_per_sec;
	}

	public void setDiskstat_sda_read_bytes_per_sec(String diskstat_sda_read_bytes_per_sec) {
		this.diskstat_sda_read_bytes_per_sec = diskstat_sda_read_bytes_per_sec;
	}

	public String getProc_run() {
		return proc_run;
	}

	public void setProc_run(String proc_run) {
		this.proc_run = proc_run;
	}

	public String getProc_total() {
		return proc_total;
	}

	public void setProc_total(String proc_total) {
		this.proc_total = proc_total;
	}

	public String getLoad_one() {
		return load_one;
	}

	public void setLoad_one(String load_one) {
		this.load_one = load_one;
	}

	public String getLoad_five() {
		return load_five;
	}

	public void setLoad_five(String load_five) {
		this.load_five = load_five;
	}

	public String getLoad_fifteen() {
		return load_fifteen;
	}

	public void setLoad_fifteen(String load_fifteen) {
		this.load_fifteen = load_fifteen;
	}

	public String getPkts_in() {
		return pkts_in;
	}

	public void setPkts_in(String pkts_in) {
		this.pkts_in = pkts_in;
	}

	public String getPkts_out() {
		return pkts_out;
	}

	public void setPkts_out(String pkts_out) {
		this.pkts_out = pkts_out;
	}

	public String getBytes_in() {
		return bytes_in;
	}

	public void setBytes_in(String bytes_in) {
		this.bytes_in = bytes_in;
	}

	public String getBytes_out() {
		return bytes_out;
	}

	public void setBytes_out(String bytes_out) {
		this.bytes_out = bytes_out;
	}

	public String getBm_id() {
		return bm_id;
	}

	public void setBm_id(String bm_id) {
		this.bm_id = bm_id;
	}

}

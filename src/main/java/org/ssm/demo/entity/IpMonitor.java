package org.ssm.demo.entity;

import java.util.Date;

public class IpMonitor {

	private Date createTime;
	private String ip;
	private String twMax;
	private String iMax;
	private String oMax;
	public Date getCreateTime() {
		return createTime;
	}
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	public String getIp() {
		return ip;
	}
	public void setIp(String ip) {
		this.ip = ip;
	}
	public String getTwMax() {
		return twMax;
	}
	public void setTwMax(String twMax) {
		this.twMax = twMax;
	}
	public String getiMax() {
		return iMax;
	}
	public void setiMax(String iMax) {
		this.iMax = iMax;
	}
	public String getoMax() {
		return oMax;
	}
	public void setoMax(String oMax) {
		this.oMax = oMax;
	}
}

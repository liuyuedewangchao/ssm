package org.ssm.demo.Thread;

import org.ssm.demo.entity.User;
import org.ssm.demo.service.IUserService;
import org.ssm.demo.utils.SpringContentUtil;

import java.util.List;
import java.util.concurrent.Callable;

import static org.ssm.demo.utils.DomParserUtils.ExportXMLMethod;

/**
 * @Author: Junerain
 * @Description:
 * @Date:Created in 9:07 2018/5/7
 * @Modified By:
 */
public class MyTaskCall implements Callable {

    private int taskIndex = 0;
    private int offset = 0;
    private int endIndex = 0;
    public String filepath = "";

    public  MyTaskCall(int num,int offset,int endIndex){
        this.taskIndex = num;
        this.offset = offset;
        this.endIndex = endIndex;
    }
    @Override
    public String call() throws Exception {
        if(taskIndex != endIndex){
            System.out.println("现在导出任务的开始下标： "+taskIndex);
            IUserService userService = SpringContentUtil.getBean("userService");//解决多线程环境中，spring无法调用service、dao的问题，自己写个获取bean的工具类
            List<User> userlist = userService.getLimit(taskIndex,offset);
            if(userlist !=null){
                filepath = ExportXMLMethod(userlist,0);
            }else{
                System.out.println(taskIndex+" 到"+(taskIndex+offset)+"的获取结果为空");
            }
            System.out.println("至 "+(taskIndex+offset)+"执行完毕");
        }else {
            System.out.println("最后一个导出任务的开始下标： "+taskIndex);
            IUserService userService = SpringContentUtil.getBean("userService");
            List<User> userlist = userService.getLimit(taskIndex,offset);
            if(userlist !=null){
                filepath = ExportXMLMethod(userlist,0);
            }else{
                System.out.println(taskIndex+"到"+(taskIndex+offset)+"的获取结果为空");
            }
            System.out.println("最后一个导出任务至 "+(taskIndex+offset)+"执行完毕");

        }
//        System.out.println(filepath);
        return filepath;
    }
}

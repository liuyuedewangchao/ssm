package org.ssm.demo.qtzjob;

import org.quartz.*;

import java.util.Date;

/**
 * @Author: Junerain
 * @Description:
 * @Date:Created in 15:21 2018/4/23
 * @Modified By:
 */
@DisallowConcurrentExecution          //用于禁止任务并发执行
public class HelloQuartz implements Job {
    @Override
    public void execute(JobExecutionContext context) throws JobExecutionException {
        JobDetail detail = context.getJobDetail();
        String name = detail.getJobDataMap().getString("name");
        System.out.println("say hello to " + name + " at " + new Date());
    }
}

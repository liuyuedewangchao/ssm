package org.ssm.demo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.ssm.demo.entity.User;
import org.ssm.demo.service.IUserService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Controller
public class UserController {

	@Autowired
	private IUserService userService;
	
	@RequestMapping(value="/showname",method=RequestMethod.POST)
//	public String showUserName(@RequestParam("uid") int uid, HttpServletRequest request, Model model){
	public void showUserName(@RequestParam("uid") int uid, HttpServletRequest request, HttpServletResponse res, Model model) throws IOException {
		System.out.println(uid);
		User user = userService.getUserById(uid);
		if(user != null){
			request.setAttribute("name", user.getUserName());
			model.addAttribute("mame", user.getUserName());
			res.getWriter().write(user.getUserName());
//			return "showName";
		}
		request.setAttribute("error", "没有找到该用户！");
//		return "error";
	}

	@RequestMapping(value="/show",method=RequestMethod.POST)
	public void show(@RequestBody @RequestParam("uid") int uid, HttpServletRequest request, HttpServletResponse res, Model model) throws IOException {
		System.out.println(uid);
		User user = userService.getUserById(uid);
		if(user != null){
			request.setAttribute("name", user.getUserName());
			model.addAttribute("mame", user.getUserName());
			res.getWriter().write(user.getUserName());
		}
		request.setAttribute("error", "没有找到该用户！");
	}
}

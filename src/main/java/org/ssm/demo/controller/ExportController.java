package org.ssm.demo.controller;

import com.mysql.jdbc.StringUtils;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.ssm.demo.service.ExportService;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.net.URLEncoder;

@RequestMapping({ "/monitor" })
@Controller
public class ExportController {
	@Resource
	private ExportService exportService;

	//新增批量导出2018.5.30
	@RequestMapping(value = "/exportselected", method = RequestMethod.GET)
	@ResponseBody
	public void listSelected(HttpServletResponse response, @RequestParam(required = false, defaultValue = "") String params,
                             @RequestParam(required = false, defaultValue = "") String params_name,
							 @RequestParam(required = false, defaultValue = "") String exportType,
                             @RequestParam(required = false, defaultValue = "") String nodeId,
                             @RequestParam(required = false, defaultValue = "") String dateType,
                             @RequestParam(required = false, defaultValue = "") String fileName) {
		try {
			if (!StringUtils.isNullOrEmpty(fileName)){
				fileName = java.net.URLDecoder.decode(fileName, "UTF-8");
			}
			response.reset();
			response.addHeader("Content-Disposition","attachment;filename=" + URLEncoder.encode(fileName, "UTF-8") + ".xls");
			response.setContentType("application/octet-stream");
			exportService.getSelectedExcel(nodeId, exportType, dateType, params,params_name,response.getOutputStream(),fileName);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	

}

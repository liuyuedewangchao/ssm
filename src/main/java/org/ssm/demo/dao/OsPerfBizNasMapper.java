package org.ssm.demo.dao;

import java.util.List;
import org.apache.ibatis.annotations.Param;
import org.ssm.demo.entity.OsPerfBizNas;
import org.ssm.demo.entity.OsPerfBizNasExample;

public interface OsPerfBizNasMapper {
    long countByExample(OsPerfBizNasExample example);

    int deleteByExample(OsPerfBizNasExample example);

    int insert(OsPerfBizNas record);

    int insertSelective(OsPerfBizNas record);

    List<OsPerfBizNas> selectByExample(OsPerfBizNasExample example);

    int updateByExampleSelective(@Param("record") OsPerfBizNas record, @Param("example") OsPerfBizNasExample example);

    int updateByExample(@Param("record") OsPerfBizNas record, @Param("example") OsPerfBizNasExample example);
}
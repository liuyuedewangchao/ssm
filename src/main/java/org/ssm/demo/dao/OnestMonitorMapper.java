package org.ssm.demo.dao;

import org.apache.ibatis.annotations.Param;
import org.ssm.demo.entity.OnestUser;

import java.util.List;

/**
 * @Author: Junerain
 * @Description:
 * @Date:Created in 9:43 2018/8/17
 * @Modified By:
 */
public interface OnestMonitorMapper {

    List<OnestUser> select_data_by_userId(
            @Param("userId") String userId,
            @Param("meter") String meter,
            @Param("time") int time
    );

    List<OnestUser> select_datas_by_userId(
            @Param("userId") String userId,
            @Param("meter") String meter,
            @Param("time") int time
    );

    OnestUser select_recentdata_by_userId(
            @Param("userId") String userId
    );
}

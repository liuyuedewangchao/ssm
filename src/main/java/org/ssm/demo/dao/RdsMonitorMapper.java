package org.ssm.demo.dao;

//import com.chinamobile.bcop.oprestmodel.console.monitor.dashboard.MonitorDashboardListResp;
import org.apache.ibatis.annotations.Param;
import org.ssm.demo.entity.RdsMonitor;

import java.util.List;

/**
 * @author junerain
 */
public interface RdsMonitorMapper {

    List<RdsMonitor> select_data_by_yesterday_and_id(
            @Param("id") String id,
            @Param("meter") String meter
    );

    List<RdsMonitor> select_data_by_today_and_id(
            @Param("id") String id,
            @Param("meter") String meter
    );

    List<RdsMonitor> select_data_by_week_and_id(
            @Param("id") String id,
            @Param("meter") String meter
    );

    List<RdsMonitor> select_data_by_halfmonth_and_id(
            @Param("id") String id,
            @Param("meter") String meter
    );

    List<RdsMonitor> select_data_by_month_and_id(
            @Param("id") String id,
            @Param("meter") String meter
    );
}

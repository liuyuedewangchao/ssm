package org.ssm.demo.dao;

import org.apache.ibatis.annotations.Param;
import org.ssm.demo.entity.User;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public interface IUserDao {
    int deleteByPrimaryKey(Integer id);

    int insert(User record);

    int insertSelective(User record);

    int insertbatch(List listhaha);

    User selectByPrimaryKey(Integer id);

    List<User> selectTen();

    List<User> selectLimit(Integer[] array);

    int updateByPrimaryKeySelective(User record);

    int updateByPrimaryKey(User record);

    User selectDifParmByPrimaryKey(String param,Integer id);

    User selectDifParmByPrimaryKey(Map<String,Object> paramters);

    User selectByNameAndPassword(@Param("userName") String userName,@Param("password") String password);

    List<User> selectTenByParam(Map<String,Object> paramters);
}
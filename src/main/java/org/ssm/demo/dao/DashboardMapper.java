package org.ssm.demo.dao;

import org.ssm.demo.entity.Dashboard;

public interface DashboardMapper {
    int deleteByPrimaryKey(String dashboardId);

    int insert(Dashboard record);

    int insertSelective(Dashboard record);

    Dashboard selectByPrimaryKey(String dashboardId);

    int updateByPrimaryKeySelective(Dashboard record);

    int updateByPrimaryKey(Dashboard record);
}
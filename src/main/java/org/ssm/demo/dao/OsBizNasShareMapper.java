package org.ssm.demo.dao;

import java.util.List;
import org.apache.ibatis.annotations.Param;
import org.ssm.demo.entity.OsBizNasShare;
import org.ssm.demo.entity.OsBizNasShareExample;

public interface OsBizNasShareMapper {
    long countByExample(OsBizNasShareExample example);

    int deleteByExample(OsBizNasShareExample example);

    int insert(OsBizNasShare record);

    int insertSelective(OsBizNasShare record);

    List<OsBizNasShare> selectByExample(OsBizNasShareExample example);

    int updateByExampleSelective(@Param("record") OsBizNasShare record, @Param("example") OsBizNasShareExample example);

    int updateByExample(@Param("record") OsBizNasShare record, @Param("example") OsBizNasShareExample example);
}
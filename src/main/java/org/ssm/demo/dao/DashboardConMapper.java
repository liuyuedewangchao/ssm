package org.ssm.demo.dao;

import org.ssm.demo.entity.DashboardCon;

public interface DashboardConMapper {
    int deleteByPrimaryKey(String dashboardConId);

    int insert(DashboardCon record);

    int insertSelective(DashboardCon record);

    DashboardCon selectByPrimaryKey(String dashboardConId);

    int updateByPrimaryKeySelective(DashboardCon record);

    int updateByPrimaryKey(DashboardCon record);
}
package org.ssm.demo.service;

import org.ssm.demo.entity.User;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public interface IUserService {

	public List<User> getLimit(int m, int n);

	public List<User> getTen();

	public User getUserById(int id);

	public void insertInfo(User user);

	public int insertbatch(List list);

	public void deleteById(int id);

//	public void updateById(int id, User user);
	public void updateById(User user);

	public User selectByDifParam(String param,int id);

	public User selectByUserNameAndPassword(String userName,String password);

	List<User> getTenByParam(Map<String,Object> paramters);
}


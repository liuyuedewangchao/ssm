package org.ssm.demo.service;

import org.springframework.stereotype.Service;
import org.ssm.demo.common.MonitorType;
import org.ssm.demo.dao.OnestMonitorMapper;
import org.ssm.demo.entity.OnestUser;

import javax.annotation.Resource;
import java.util.List;

@Service
public class OnestMonitorService {

  @Resource
  private OnestMonitorMapper onestMonitorMapper;

    public List<OnestUser> getDataFromOnest(String dateType, String userId, String meter){
        int time = 0;
        if(dateType.equals(MonitorType.YESTERDAY)){time = 1;
        }else if(dateType.equals(MonitorType.WEEK)){time = 7;
        }
        return onestMonitorMapper.select_data_by_userId(userId,meter,time);
    }

    public List<OnestUser> getDataFromOnest_hour(String dateType, String userId, String meter) {
        int time = 0;
        if(dateType.equals(MonitorType.HALFMONTH)){time = 14;
        }else if(dateType.equals(MonitorType.MONTH)){time = 30;
        }
        return onestMonitorMapper.select_datas_by_userId(userId,meter,time);
    }

    public OnestUser getRecentDataFromOnest(String userId){
        return onestMonitorMapper.select_recentdata_by_userId(userId);
    }
  
}

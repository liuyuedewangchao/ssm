package org.ssm.demo.service;

import org.springframework.stereotype.Service;
import org.ssm.demo.common.MonitorType;
import org.ssm.demo.dao.RdsMonitorMapper;
import org.ssm.demo.entity.RdsMonitor;

import javax.annotation.Resource;
import java.util.List;

@Service
public class RdsMonitorService {

    @Resource
    RdsMonitorMapper rdsMonitorMapper;
  /**
   * 查询ip 时间段的性能数据
   * @param id  公网ip
   * @param type 昨天 ，今天，一周，一个月
   * @return
   */
  public List<RdsMonitor> getDataById(String id, String type, String meter) {
    switch (type) {
        case MonitorType.YESTERDAY:
            return this.rdsMonitorMapper.select_data_by_yesterday_and_id(id,meter);
        case MonitorType.TODAY:
            return this.rdsMonitorMapper.select_data_by_today_and_id(id,meter);
        case MonitorType.WEEK:
            return this.rdsMonitorMapper.select_data_by_week_and_id(id,meter);
        case MonitorType.HALFMONTH:
            return this.rdsMonitorMapper.select_data_by_halfmonth_and_id(id,meter);
        case MonitorType.MONTH:
            return this.rdsMonitorMapper.select_data_by_month_and_id(id,meter);
	}
	return null;
  }
 
}

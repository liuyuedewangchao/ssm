package org.ssm.demo.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.ssm.demo.dao.DashboardMapper;
import org.ssm.demo.dao.IUserDao;
import org.ssm.demo.entity.Dashboard;
import org.ssm.demo.entity.User;

import java.util.Date;
import java.util.List;

/**
 * @Author: Junerain
 * @Description:
 * @Date:Created in 16:44 2018/8/15
 * @Modified By:
 */
@Service
public class DashboardService {

    @Autowired
    public DashboardMapper dashboardMapper;

    public int insert(Dashboard dashboard){
        return dashboardMapper.insert(dashboard);
    }
}

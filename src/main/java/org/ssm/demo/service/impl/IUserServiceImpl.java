package org.ssm.demo.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.ssm.demo.dao.IUserDao;
import org.ssm.demo.entity.User;
import org.ssm.demo.service.IUserService;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service("userService")
public class IUserServiceImpl  implements IUserService{

	@Autowired
	public IUserDao udao;

	@Override
	public List<User> getTen() {
		return udao.selectTen();
	}

	@Override
	public List<User> getLimit(int m, int n){
		Integer[] array = new Integer[]{m,n};
		return udao.selectLimit(array);
	}

	@Override
	public User getUserById(int id) {
		return udao.selectByPrimaryKey(id);
	}

	@Override
	public void insertInfo(User user){
		udao.insert(user);
	}

	@Override
	public int insertbatch(List list){
		return udao.insertbatch(list);
	}

	@Override
    public void deleteById(int id){
	    udao.deleteByPrimaryKey(id);
    }

    @Override
    public void updateById(User user){
//	    User user1 = udao.selectByPrimaryKey(id);
	    udao.updateByPrimaryKey(user);
    }

	@Override
	public User selectByDifParam(String param, int id) {
		Map<String, Object> paramters = new HashMap<String, Object>();
		paramters.put("meter", param);
		paramters.put("id", id);
		return udao.selectDifParmByPrimaryKey(paramters);
	}

	@Override
	public User selectByUserNameAndPassword(String userName, String password) {
		return udao.selectByNameAndPassword(userName,password);
	}

	@Override
	public List<User> getTenByParam(Map<String, Object> paramters) {
		return udao.selectTenByParam(paramters);
	}

}

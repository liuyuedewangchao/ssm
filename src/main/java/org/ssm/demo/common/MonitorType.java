package org.ssm.demo.common;

public class MonitorType {

	public static final String YESTERDAY = "yesterday";
	public static final String TODAY = "today";
	public static final String WEEK = "week";
	public static final String HALFMONTH = "halfmonth";
	public static final String MONTH = "month";
}

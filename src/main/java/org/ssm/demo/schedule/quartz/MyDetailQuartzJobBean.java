package org.ssm.demo.schedule.quartz;

import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.context.ApplicationContext;
import org.springframework.scheduling.quartz.QuartzJobBean;

import java.lang.reflect.Method;

/**
 * @Author: Junerain
 * @Description:
 * @Date:Created in 10:10 2018/5/11
 * @Modified By:
 */
public class MyDetailQuartzJobBean extends QuartzJobBean {

    private String targetObject;
    private String targetMethod;
    private ApplicationContext ctx;

    protected void executeInternal(JobExecutionContext context)throws JobExecutionException {
        try {
            Object otargetObject = ctx.getBean(targetObject);
            Method m = null;
            try {
                m = otargetObject.getClass().getMethod(targetMethod,new Class[] {});
                m.invoke(otargetObject, new Object[] {});
            } catch (SecurityException e) {
            } catch (NoSuchMethodException e) {
            }
        } catch (Exception e) {
            throw new JobExecutionException(e);
        }

    }

    public void setApplicationContext(ApplicationContext applicationContext){
        this.ctx=applicationContext;
    }

    public void setTargetObject(String targetObject) {
        this.targetObject = targetObject;
    }



    public void setTargetMethod(String targetMethod) {
        this.targetMethod = targetMethod;
    }

}

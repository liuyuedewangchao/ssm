package org.ssm.demo.service.redis;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.ssm.demo.cache.RedisCacheManager;
import redis.clients.jedis.Jedis;

/**
 * @Author: Junerain
 * @Description:
 * @Date:Created in 20:26 2018/5/7
 * @Modified By:
 */
/**
 * 配置spring和junit整合，junit启动时加载springIOC容器 spring-test,junit
 */
@RunWith(SpringJUnit4ClassRunner.class)
// 告诉junit spring配置文件
@ContextConfiguration({ "classpath:applicationContext.xml"})
public class RedisTest {

    @Autowired
    private RedisCacheManager redisCacheManager;

    private static boolean isStart = false;

    @Test
    public void testRedis1() {
        Jedis jj = new  Jedis("localhost");
        jj.set("key1", "I am value 1");
        String ss = jj.get("key1");
        System.out.println(ss);
    }

    @Test
    public void testRedis2() {
        String str = "hello world!";
        redisCacheManager.set("phil_token", str, 60); //1小时
        System.out.println(redisCacheManager.get("phil_token"));
    }

}

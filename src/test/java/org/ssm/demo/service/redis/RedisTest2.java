package org.ssm.demo.service.redis;

/**
 * @Author: Junerain
 * @Description:
 * @Date:Created in 15:51 2018/5/8
 * @Modified By:
 */

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.util.JsonPathExpectationsHelper;
import org.ssm.demo.cache.RedisCacheManager;
import org.ssm.demo.entity.User;
import org.ssm.demo.service.IUserService;
import redis.clients.jedis.Jedis;

import java.util.ArrayList;
import java.util.List;

/**
 * 配置spring和junit整合，junit启动时加载springIOC容器 spring-test,junit
 */
@RunWith(SpringJUnit4ClassRunner.class)
// 告诉junit spring配置文件
@ContextConfiguration({ "classpath:applicationContext.xml"})
public class RedisTest2 {

    @Autowired
    private RedisCacheManager redisCacheManager;

    @Autowired
    private IUserService iUserService;

    private static boolean isStart = false;

    @Test
    public void testRedis1() {
        Jedis jj = new  Jedis("localhost");
        jj.set("key1", "I am value 1");
        String ss = jj.get("key1");
        System.out.println(ss);
    }

    @Test
    public void testRedis2() {
//        String str = "hello world!";
//        redisCacheManager.set("phil_token", str, 60); //1小时
//        System.out.println(redisCacheManager.get("phil_token"));

        List<User> list = new ArrayList<>();
        list = iUserService.getLimit(0,100000);
        String jsonlist = JSON.toJSONString(list);//将obect转换为json格式的字符串

        JSONArray array = JSON.parseArray(jsonlist);

        List<String> liststr = new ArrayList<>();


        for(int i = 0;i<array.size();i++){
//            System.out.println(array.get(i).toString());
            JSONObject jo = array.getJSONObject(i);
//            redisCacheManager.set(jo.getString("id"),jo.toJSONString());
            liststr.add(jo.toJSONString().toString());
            redisCacheManager.lSet("userlist",jo.toJSONString());
        }
//        redisCacheManager.lSet("userlist",liststr);

    }

}

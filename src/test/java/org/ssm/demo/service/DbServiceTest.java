package org.ssm.demo.service;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.ssm.demo.entity.Dashboard;
import org.ssm.demo.entity.OnestUser;
import org.ssm.demo.entity.RdsMonitor;
import org.ssm.demo.entity.User;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

/**
 * 配置spring和junit整合，junit启动时加载springIOC容器 spring-test,junit
 */
@RunWith(SpringJUnit4ClassRunner.class)
// 告诉junit spring配置文件
@ContextConfiguration({ "classpath:applicationContext.xml"})
public class DbServiceTest {

	@Autowired
	public DashboardService dashboardService;

	@Autowired
	private RdsMonitorService rdsMonitorService;

	@Autowired
	private OnestMonitorService onestMonitorService;

	@Test
	public void insertData(){
		Dashboard db = new Dashboard();
		db.setDashboardId(UUID.randomUUID().toString().replaceAll("-", ""));
		db.setDashboardName("大盘一");

		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Date createTime = null;
		try {
			createTime = formatter.parse(formatter.format(new Date()));
		} catch (ParseException e) {
			e.printStackTrace();
		}

		db.setCreateTime(createTime);
		db.setCustomerId("123456qwe");
		db.setUserId("123456qwe");
		db.setStatus(0);
		System.out.println("开始插入");
		dashboardService.insert(db);
		System.out.println("插入结束");

	}

	@Test
	public void testGetOnestMonitorData(){
		String id = "078ffc536b1742c49d7f913d9c78f660";
		String meter = "OBJ_QTY";
		String type = "today";
		List<OnestUser> onestUsers = onestMonitorService.getDataFromOnest(type,id,meter);
		System.out.println(onestUsers.size());
	}

	@Test
	public void testGetRdsMonitorData(){
		String id = "rdstest02-member-100_9d6e3e3f-61f1-4b9c-a646-ba3606a5f8d6";
		String meter = "CPU_LOAD";
		String type = "today";
		List<RdsMonitor> rdsMonitors = rdsMonitorService.getDataById(id,type,meter);
		System.out.println(rdsMonitors.size());
	}

	@Test
	public void testStr() throws UnsupportedEncodingException {

		String str = "[\"内存的使用率（%）\",\"可用内存量(bytes)\"]";
		str = java.net.URLEncoder.encode(str, "UTF-8");
		System.out.println(str);
//		String str = "[\"123\",\"456\",\"789\"]";
//		String str = "[%22%E5%86%85%E5%AD%98%E7%9A%84%E4%BD%BF%E7%94%A8%E7%8E%87%EF%BC%88%%EF%BC%89%22,%22%E5%8F%AF%E7%94%A8%E5%86%85%E5%AD%98%E9%87%8F(bytes)%22]";
		try {
			str = java.net.URLDecoder.decode(str,"UTF-8");
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		System.out.println(str);
		String str1 = str.substring(2,str.length()-2);
		System.out.println(str1);
//		String[] str2 = str1.split("\",\"");

		String[] str2 = str.substring(2,str.length()-2).split("\",\"");
		for (String str0 : str2) {
			System.out.println(str0);
		}
	}

	@Test
	public void testURLcode() throws UnsupportedEncodingException {
		String name=java.net.URLEncoder.encode("测试", "UTF-8");
		System.out.println(name);
		name=java.net.URLEncoder.encode(name,"UTF-8");
		System.out.println(name);
		name=java.net.URLDecoder.decode(name, "UTF-8");
		System.out.println(name);
		System.out.println(java.net.URLDecoder.decode(name, "UTF-8"));
	}

	@Test
	public void testJson(){
		try{
			String str = "{'list':['1,2','3,4','5,6']}";
			JSONObject jsonObject = JSON.parseObject(str);  //吧转为json对象
			String array= jsonObject.getString("list"); //获取list的值
			JSONArray jsonArray = JSONArray.parseArray(array); //吧list的值转为json数组对象
			Object[] strs = jsonArray.toArray(); //json转为数组
			for(Object s:strs){
				System.out.println(s);
			}
			for(int i = 0;i<strs.length;i++){
				System.out.println(strs[0]);
			}
		}catch(Exception e){e.printStackTrace();}
	}

	@Test
	public void testJson2(){
		try{
			String str = "{\"meterList\":[{\"meter\":\"MEM_USED\",\"meterDesc\":\"中国\"},{\"meter\":\"MEM_USED\",\"meterDesc\":\"美国\"}]}";
			JSONObject jsonObject = JSON.parseObject(str);  //吧转为json对象
			String array= jsonObject.getString("meterList"); //获取list的值
			JSONArray jsonArray = JSONArray.parseArray(array); //吧list的值转为json数组对象
			Object[] strs = jsonArray.toArray(); //json转为数组
			for(Object s:strs){
				JSONObject jsonObject1 = JSON.parseObject(s.toString());
				System.out.println(jsonObject1.get("meter"));
				System.out.println(jsonObject1.get("meterDesc"));
			}
//			for(int i = 0;i<strs.length;i++){
//				System.out.println(strs[0]);
//			}
		}catch(Exception e){e.printStackTrace();}
	}






}

package org.ssm.demo.service;

import org.apache.poi.ss.formula.functions.T;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.omg.PortableServer.LIFESPAN_POLICY_ID;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.ssm.demo.common.MonitorType;
import org.ssm.demo.entity.RdsMonitor;
import org.ssm.demo.entity.User;

import java.lang.reflect.Field;
import java.util.*;

/**
 * 配置spring和junit整合，junit启动时加载springIOC容器 spring-test,junit
 */
@RunWith(SpringJUnit4ClassRunner.class)
// 告诉junit spring配置文件
@ContextConfiguration({ "classpath:applicationContext.xml"})
public class IUserServiceTest {

	@Autowired
	public IUserService userService;
	
	@Test
	public void getUserByIdTest(){
		User user = userService.getUserById(1);
		System.out.println(user.getUserName());
	}

	@Test
	public void getTenUser(){
		List<User> userlist = userService.getTen();
		System.out.println(userlist.get(0).getAge());
	}

	@Test
	public void getLimit(){
		List<User> userList = userService.getLimit(0,200);
		System.out.println(userList.get(0).getAge()+"  what?");
	}

	@Test
	public void insertData(){
		User u = new User();
		u.setId(5);
		u.setUserName("junerain");
		u.setPassword("123456");
		u.setAge(26);
		System.out.println("开始插入");
		userService.insertInfo(u);
		System.out.println("插入结束");
//		User user = userService.insertInfo(u);

	}

	@Test
	public void insertBatch(){
		List<User> list = new ArrayList<>();
		int num = 97;
		for(int i=3; i<10; i++){
			User u = new User();
			u.setId(i);
			u.setUserName("junerain");
			u.setPassword("123456");
			u.setAge(26);
			list.add(u);
		}
		System.out.println(list.get(list.size()-1).getId());
		userService.insertbatch(list);
	}

	@Test
    public void deleteTest(){
        System.out.println("删除插入");
	    userService.deleteById(4);
        System.out.println("删除结束");
    }

    @Test
    public void updateTest(){
	    User u = new User();
	    u.setId(1);
	    u.setAge(11);
	    u.setUserName("lalala");
	    u.setPassword("123456");
        System.out.println("开始更新");
	    userService.updateById(u);
	    System.out.println("结束更新");
    }

    @Test
	public void testSelectByDifParamAndId(){
		String param = "user_name";
		int id = 1;
		User user = userService.selectByDifParam(param,id);
		System.out.println(user.getAge());
	}

	@Test
	public void selectByUserNameAndPassword(){
		String userName = "王宇";
		String password = "123456";
		User user = userService.selectByUserNameAndPassword(userName,password);
		System.out.println(user.getAge());
	}

	@Test
	public void test1(){
		List<User> userlist = userService.getLimit(10,2);
		Map<String, Object> paramters = new HashMap<>();
		paramters.put("userlist", userlist);
		paramters.put("password","123456");
		System.out.println(userlist.size());

		List<User> userlists = userService.getTenByParam(paramters);
		System.out.println(userlists.get(0).getUserName());
	}

	@Test
	public void test2(){
//		List<T> list0 = new ArrayList<>();
//		List<User> userList = userService.getLimit(0,10);
//		list0 = userList;

		List<Object> list = new ArrayList<>();
		List<User> userlist = userService.getLimit(0,10);
		if(userlist != null && userlist.size()>0){
			for(int i = 0;i<userlist.size();i++){
				list.add(userlist.get(i));
			}
		}

		try {
			System.out.println(getFieldValueByObject(list.get(0),"userName").toString());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Test
	public void testDate(){
		String dateType = "week";
		int date = 0;
		if(dateType.equals("yesterday")){date = 1;
		}else if(dateType.equals("week")){date = 7;
		}else if(dateType.equals("month")){date = 30;
		}
		System.out.println(date);

	}

	public static Object getFieldValueByObject (Object object , String targetFieldName) throws Exception {

		// 获取该对象的Class
		Class objClass = object.getClass();
		// 获取所有的属性数组
		Field[] fields = objClass.getDeclaredFields();
		for (Field field:fields) {
			field.setAccessible(true);
			// 属性名称
			String currentFieldName = "";
			// 获取属性上面的注解 import com.fasterxml.jackson.annotation.JsonProperty;
			/**
			 *  举例：
			 *   @JsonProperty("di_ren_jie")
			 *   private String diRenJie;
			 */
//			boolean has_JsonProperty =  field.isAnnotationPresent(JsonProperty.class);
//			if(has_JsonProperty){
//				currentFieldName = field.getAnnotation(JsonProperty.class).value();
//			}else {
				currentFieldName = field.getName();
//			}
			if(currentFieldName.equals(targetFieldName)){
				return field.get(object); // 通过反射拿到该属性在此对象中的值(也可能是个对象)
			}
		}
		return null;
	}

	@Test
	public void testlistdata(){
		User u = new User();
		u.setId(1);
		u.setUserName("lalala");
		User u1 = new User();
		u1.setId(2);
		u1.setUserName("gagaga");
		List<User> userlist = new ArrayList<>();
		userlist.add(u);
		userlist.add(u1);

		List<Object> list = new ArrayList<>();
		if(userlist != null && userlist.size()>0){
			for(int i = 0;i<userlist.size();i++){
				list.add(userlist.get(i));
			}
		}
		String meter = "id";
		List<String> strs = getMeterData(list,meter);

		for(String str:strs){
			System.out.println(str);
		}
	}

	public static List<String> getMeterData(List<Object> objlist,String meter){
		List<String> reslist = new ArrayList<>();
		for(Object obj : objlist){
			Field[] fields = obj.getClass().getDeclaredFields();
			for (Field field:fields) {
				field.setAccessible(true);
				Object meterData = null;
				try {
					meterData = field.get(obj); // 通过反射拿到该属性在此对象中的值(也可能是个对象)
				} catch (IllegalAccessException e) {
					e.printStackTrace();
				}
				if(meterData != null && !field.getName().equals(meter)){
					reslist.add(meterData.toString());
				}
			}
		}
		return reslist;
	}

}

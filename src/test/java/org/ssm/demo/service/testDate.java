package org.ssm.demo.service;

import org.apache.http.client.utils.DateUtils;
import org.junit.Test;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * @Author: Junerain
 * @Description:
 * @Date:Created in 16:40 2018/7/26
 * @Modified By:
 */
public class testDate {
    public static void main(String args[]){
        String dstr = "Thu Jul 26 13:16:28 CST 2018";
        Date date = DateUtils.parseDate(dstr, new String[]{"yyyy-MM-dd HH:mm:ss"});

        Date now = new Date();
        Map<Object,Object> m = new  HashMap<>();
        m.put("time",now);

        Date date1 = (Date) m.get("time");



        long x = now.getTime() - date1.getTime();
        System.out.println(x);
        if(x <= 10 * 60 * 1000) {
            System.out.println("可以，在时间范围内");
        }else{
            System.out.println("NO！不好意思，超时了");
        }
    }

    @Test
    public void testMap(){
        Map<Object,Object> monitor = new HashMap<>();
        String resourceType = "02";
//        if(resourceType.equals("02") && monitor == null){
        if(resourceType.equals("02") && (monitor == null || monitor.size()<1)){
            System.out.println("终于通了啊");
        }else{
            System.out.println("没有通");
        }
    }

    @Test
    public void testDate1(){
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date createTime = null;
        try {
            createTime = formatter.parse(formatter.format(new Date()));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        String time = formatter.format(createTime);
        System.out.println("时间是" + time);
    }

    @Test
    public void testVm(){
        String selectType="publicIp";
        String customerId = "dsfsdfsd";
        String userId = "sf2fsvfdf";
        String filter = "10.63.68.123";
        int page = 1;
        int size = 10;
        Map<String,Object> parameters = new HashMap<String,Object>();
        page = page < 1 ? 0 : (page - 1);
        parameters.put("customerId", customerId);
        parameters.put("userId", userId);
        if(!selectType.trim().equals("") && !filter.trim().equals("")) {
            if(selectType.equals("name"))
                parameters.put("name", "%"+filter+"%");
            else if(selectType.equals("id"))
                parameters.put("id", "%"+filter+"%");
            else if(selectType.equals("publicIp"))
                parameters.put("publicIp", "%"+filter+"%");
            else if(selectType.equals("privateIp"))
                parameters.put("privateIp", "%"+filter+"%");
        }
        parameters.put("pageIndex", page * size);
        parameters.put("pageSize", size);
    }

    @Test
    public void getCurrentTimePlusHourIntegralTime() {
        String time = "2018-09-03 09:30:00";
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");//yyyy-mm-dd, 会出现时间不对, 因为小写的mm是代表: 秒
        Date date = new Date();
        try {
            date = sdf.parse(time);
        } catch (ParseException e) {
            e.printStackTrace();
        }

//        Date date = new Date();
        int hour = -1;
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        if (hour != 0) {
            cal.add(Calendar.HOUR, hour);
        }
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);
        System.out.println(cal.getTime());
    }

    @Test
    public void testNewData(){
        Date date = new Date();
        System.out.println(getCurrentTimePlusHourIntegralTime(date,0));
    }

    public static Date getCurrentTimePlusHourIntegralTime(Date date, int hour) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        if (hour != 0) {
            cal.add(Calendar.HOUR, hour);
        }
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);
        return cal.getTime();
    }

}

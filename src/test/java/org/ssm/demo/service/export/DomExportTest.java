package org.ssm.demo.service.export;

/**
 * @Author: Junerain
 * @Description:
 * @Date:Created in 17:46 2018/4/28
 * @Modified By:
 */
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.ssm.demo.entity.User;
import org.ssm.demo.service.IUserService;
import org.ssm.demo.utils.DomParserUtils;
import org.dom4j.DocumentException;
import org.junit.Test;

import java.util.List;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration({ "classpath:applicationContext.xml"})
public class DomExportTest {
    @Autowired
    public IUserService userService;
    @Test
    public void testParseMethod1() throws DocumentException
    {
        DomParserUtils.parseMethod1();
    }

    @Test
    public void testParseMethod2() throws DocumentException
    {
        DomParserUtils.parseMethod2();
    }

    @Test
    public void testGenerate()
    {
        DomParserUtils.generate();
    }

    @Test
    public  void testExportXML(){
        List<User> userlist = null;
        DomParserUtils.ExportXMLMethod(userlist,0);
    }



}

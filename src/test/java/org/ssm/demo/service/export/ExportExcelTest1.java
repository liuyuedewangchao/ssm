package org.ssm.demo.service.export;

import java.lang.String;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.ssm.demo.entity.User;
import org.ssm.demo.service.IUserService;
import org.ssm.demo.utils.ExcelUtil;

import javax.servlet.jsp.tagext.PageData;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.List;

/**
 * @Author: Junerain
 * @Description:
 * @Date:Created in 10:43 2018/5/15
 * @Modified By:
 */
@RunWith(SpringJUnit4ClassRunner.class)
// 告诉junit spring配置文件
@ContextConfiguration({ "classpath:applicationContext.xml"})
public class ExportExcelTest1 {

    @Autowired
    public IUserService userService;

    @Test
    public void testExportExcel(){
        //总数
        int count = 700;
        //获取数据
//        List<User> list = userService.getTen();
        List<User> list = userService.getLimit(0,count);

        //excel标题
        java.lang.String[] title = {"ID","用户名","密码","年龄"};

        //excel文件名
        String fileName = "用户表"+System.currentTimeMillis()+".xls";

        //sheet名
        String sheetName = "用户表";

        //内容
        String[][] content = new String[list.size()][];

        long startTime1 = System.currentTimeMillis();

        for(int i = 0;i<list.size();i++){
            content[i] = new String[title.length];
            User obj = list.get(i);
            content[i][0] = obj.getId().toString();
            content[i][1] = obj.getUserName();
            content[i][2] = obj.getPassword();
            content[i][3] = obj.getAge().toString();
        }

        //导出excel
        XSSFWorkbook wb = ExcelUtil.getXSSFWorkbook_testSheet(fileName,sheetName, title, content, null);

        long endTime1 = System.currentTimeMillis();
        System.out.println("导出程序合计运行时间：" + (endTime1 - startTime1) + "ms");//输出程序运行时间
    }

}

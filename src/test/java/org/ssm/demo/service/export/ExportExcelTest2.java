package org.ssm.demo.service.export;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.ssm.demo.Thread.MyTaskExportExcelCall;
import org.ssm.demo.service.IUserService;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.*;

import static org.ssm.demo.utils.ThreadUtils.getIndex;

/**
 * @Author: Junerain
 * @Description:
 * @Date:Created in 9:20 2018/5/21
 * @Modified By:
 */
@RunWith(SpringJUnit4ClassRunner.class)
// 告诉junit spring配置文件
@ContextConfiguration({ "classpath:applicationContext.xml"})
public class ExportExcelTest2 {

    @Autowired
    public IUserService userService;

    @Test
    public void getLimitAndExportExcel() throws ExecutionException, InterruptedException {
//        String fileName = "用户表"+System.currentTimeMillis()+".xls";
        String fileName = "用户表";
        String fileName1 = "";
        String sheetName = "用户表";
        String[] title = {"ID","用户名","密码","年龄"};

        int minSize = 100;//单个线程最小执行数量
        int maxTaskNum = 5;//最大线程数
        int count = 100000;//总数
        int[] indexs = getIndex(count,minSize,maxTaskNum);
        int offset = indexs[1] - indexs[0];
        int endIndex = indexs[indexs.length-2];

        Map<Integer,HSSFWorkbook> map = new HashMap<Integer,HSSFWorkbook>();
        List<Future<HSSFWorkbook>> futures = new ArrayList<>();

        //多线程
        long startTime1 = System.currentTimeMillis();
        ThreadPoolExecutor executor = (ThreadPoolExecutor) Executors.newCachedThreadPool();
        for(int i=0;i<indexs.length-1;i++){
//            MyTask myTask = new MyTask(indexs[i],offset,endIndex);
//            executor.execute(myTask);
            fileName1 = fileName+System.currentTimeMillis()+"_"+String.valueOf(i+1);
            futures.add(executor.submit(new MyTaskExportExcelCall(indexs[i],offset,endIndex,fileName1,sheetName,title)));
        }
        executor.shutdown();
        try {//等待直到所有任务完成
            executor.awaitTermination(Long.MAX_VALUE, TimeUnit.MINUTES);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        long endTime1 = System.currentTimeMillis();
        System.out.println("导出程序合计运行时间：" + (endTime1 - startTime1) + "ms");//输出程序运行时间

        for (int i = 0;i<futures.size();i++) {
            map.put(i,futures.get(i).get());
        }

//        long startTime2 = System.currentTimeMillis();
//        getOneXML(map);//合并各个xml文件
//        long endTime2 = System.currentTimeMillis();
//        System.out.println("合并程序运行时间：" + (endTime2 - startTime2) + "ms");//输出程序运行时间
    }
}

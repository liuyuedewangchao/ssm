package org.ssm.demo.service;

import org.apache.commons.lang3.StringUtils;
import org.springframework.context.ApplicationContext;
import org.ssm.demo.utils.SFTPHelperUtil;

import java.util.HashMap;
import java.util.Map;

/**
 * @Author: Junerain
 * @Description:
 * @Date:Created in 17:04 2018/7/17
 * @Modified By:
 */
public class testFileName {
    /***
     * SFTP操作者
     */
    private static SFTPHelperUtil sftpHelperUtil;

    public static void main(String[] args) {
        String filename1 = "CSMP3243431313546845.txt";
        String filename2 = "20180703190840-2988-VMPerformance.txt";
        String[] f1 = filename1.split("-");
        String[] f2 = filename2.split("-");
        System.out.println(String.valueOf(f1.length));
        System.out.println(String.valueOf(f2.length));
        System.out.println(f1[0]);
        sftpHelperUtil = new SFTPHelperUtil();
        validMoniterFtp(1);
        System.out.println(sftpHelperUtil.getServerIp());

        Map<String,Object> param = new HashMap<String,Object>();
        param.put("a","111111111111111");
        param.put("b","222222222222222");
        param.put("a","333333333333333");
        System.out.println(param.get("a"));
    }

    public static boolean validMoniterFtp(int ftpNumber) {
        String serverIp = "clm.ftp.bcop.com";
        String port = "22";
        System.out.println("配置文件获取的基本参数：" + serverIp + "-" +port);

        String fileTransferType = "SFTP";
        if(fileTransferType.equals("SFTP")){
            System.out.println("配置文件获取到的传输协议类型：" + fileTransferType);
            if (StringUtils.isBlank(serverIp)) {
                System.out.println("设置的sftp" + ftpNumber + "服务器IP为空，跳过此SFTP服务器的采集");
                return false;
            } else {
                System.out.println(serverIp);
                sftpHelperUtil.setServerIp(serverIp);
            }
        }
        return true;

    }
}

package org.ssm.demo.service;

import org.quartz.JobDetail;
import org.quartz.Scheduler;
import org.quartz.Trigger;
import org.quartz.impl.StdSchedulerFactory;
import org.ssm.demo.qtzjob.HelloQuartz;

import static org.quartz.JobBuilder.newJob;
import static org.quartz.SimpleScheduleBuilder.simpleSchedule;
import static org.quartz.TriggerBuilder.newTrigger;

/**
 * @Author: Junerain
 * @Description:
 * @Date:Created in 15:22 2018/4/23
 * @Modified By:
 */
public class QuartzTest {
    public static void main(String[] args) {
        try {
            //创建scheduler
            Scheduler scheduler = StdSchedulerFactory.getDefaultScheduler();

            //定义一个JobDetail
            JobDetail job1 = newJob(HelloQuartz.class) //定义Job类为HelloQuartz类，这是真正的执行逻辑所在
                    .withIdentity("job1", "group1") //定义name/group
                    .usingJobData("name", "王宇") //定义属性
                    .build();

            //定义一个Trigger1
            Trigger trigger1 = newTrigger().withIdentity("trigger1", "group1") //定义name/group
                    .startNow()//一旦加入scheduler，立即生效
                    .withSchedule(simpleSchedule() //使用SimpleTrigger
                            .withIntervalInSeconds(1) //每隔一秒执行一次
                            .repeatForever()) //一直执行，奔腾到老不停歇
                    .build();

            //定义一个JobDetail
            JobDetail job2 = newJob(HelloQuartz.class) //定义Job类为HelloQuartz类，这是真正的执行逻辑所在
                    .withIdentity("job2", "group1") //定义name/group
                    .usingJobData("name", "junerain") //定义属性
                    .build();

            //定义一个Trigger2
            Trigger trigger2 = newTrigger().withIdentity("trigger2", "group1") //定义name/group
                    .startNow()//一旦加入scheduler，立即生效
                    .withSchedule(simpleSchedule() //使用SimpleTrigger
                            .withIntervalInSeconds(2) //每隔2秒执行一次
//                            .repeatForever()) //一直执行，奔腾到老不停歇
                            .withMisfireHandlingInstructionFireNow())
                    .build();

            //加入这个调度
            scheduler.scheduleJob(job1, trigger1);
            scheduler.scheduleJob(job2, trigger2);

            //启动之
            scheduler.start();

            //运行一段时间后关闭
            Thread.sleep(5000);
            scheduler.shutdown(true);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}

package org.ssm.demo.service;

import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * @Author: Junerain
 * @Description:
 * @Date:Created in 17:53 2018/4/23
 * @Modified By:
 */
public class QuartzTest2 {
    public static void main(String[] args) throws IOException {
        // TODO Auto-generated method stub
        ApplicationContext context=new ClassPathXmlApplicationContext("applicationContext.xml");
        System.out.println("启动Task:");
        System.out.println("请输入 exit 结束Task:");

//        Scheduler scheduler=(Scheduler)context.getBean("SpringJobSchedulerFactoryBean");
        Scheduler scheduler=(Scheduler)context.getBean("baseQuartzScheduler");

        BufferedReader reader=new BufferedReader(new InputStreamReader(System.in));
        while(true){
            if(reader.readLine().equals("exit")) break;
        }
//        try {
//            Thread.sleep(1000);
//        } catch (InterruptedException e) {
//            e.printStackTrace();
//        }
        try {
            scheduler.shutdown();
        } catch (SchedulerException e) {
            e.printStackTrace();
        }
    }
}
